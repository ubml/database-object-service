/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.generator.field;

import java.util.ArrayList;

import io.iec.edp.caf.generator.baseInfo.AnnotationInfo;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.Modifier.ModifierKeyword;

import io.iec.edp.caf.generator.BaseGenerator;
import io.iec.edp.caf.generator.BaseGeneratorContext;
import io.iec.edp.caf.generator.baseInfo.FieldGenerateInfo;
import io.iec.edp.caf.generator.baseInfo.TypeInfo;
import io.iec.edp.caf.generator.item.ItemInfoGenerator;

/**
 * @author liu_wei
 */
public abstract class FieldGenerator extends BaseGenerator {

    protected FieldGeneratorContext getContext() {
        return (FieldGeneratorContext) super.getContext();
    }

    @Override
    protected BaseGeneratorContext createContext() {
        FieldGeneratorContext context = new FieldGeneratorContext();
        context.fieldInfo = createFieldInfo();
        return context;
    }

    private FieldGenerateInfo createFieldInfo() {
        FieldGenerateInfo info = FieldGenerateInfo.create();
        info.setAst(ast);
        return info;
    }

    @Override
    protected void doInitialize() {

    }

    @Override
    protected void beforeGenerate() {
        getContext().fieldInfo.setFieldName(getFieldName());

        TypeInfo typeInfo = getFieldType();
        getContext().fieldInfo.setFieldType(typeInfo);

        String typeFullName = typeInfo.getTypeFullName();
        if (typeFullName != null)
            addImport(typeFullName);

        getContext().fieldInfo.setModifiers(getAccessModifier());

        getContext().fieldInfo.setInitializer(buildGetMethodBody());
        ArrayList<AnnotationInfo> attrs = getAttributeList();
        getContext().fieldInfo.setAttributes(attrs);
        if (attrs != null) {
            for (AnnotationInfo attr :
                    attrs) {
                String typeFullName1 = attr.getTypeInfo().getTypeFullName();
                if (typeFullName1 != null)
                    addImport(typeFullName1);
            }
        }

    }

    @Override
    protected void doGenerate() {
        getContext().fieldInfo.generate();
    }

    @Override
    protected final void afterGenerate() {
        ((ItemInfoGenerator) parentGenerator).addChildSyntax(getContext().fieldInfo.getFiledResult());
    }

    protected abstract TypeInfo getFieldType();

    protected abstract String getFieldName();

    protected abstract ArrayList<ModifierKeyword> getAccessModifier();

    protected Expression buildGetMethodBody() {
        return null;
    }

    protected abstract ArrayList<AnnotationInfo> getAttributeList();

}
