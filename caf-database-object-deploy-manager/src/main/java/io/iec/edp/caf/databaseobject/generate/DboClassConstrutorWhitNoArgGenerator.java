/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.generate;

import io.iec.edp.caf.databaseobject.api.entity.DatabaseObjectTable;
import io.iec.edp.caf.generator.baseInfo.ParameterInfo;
import io.iec.edp.caf.generator.baseInfo.TypeInfo;
import io.iec.edp.caf.generator.method.ClassMethodGenerator;
import org.eclipse.jdt.core.dom.*;

import java.util.ArrayList;

/**
 * @author liu_wei
 */
public class DboClassConstrutorWhitNoArgGenerator extends ClassMethodGenerator {
    private DatabaseObjectTable table;

    public DboClassConstrutorWhitNoArgGenerator(DatabaseObjectTable databaseObject) {
        this.table = databaseObject;
    }


    @Override
    protected boolean buildIsConstructor() {
        return true;
    }

    @Override
    protected Block buildMethodBody() {
        Block block = ast.newBlock();
        return block;
    }

    private void addSetContextArguments(MethodInvocation setContextInvocation, String varName) {
    }

    @Override
    protected String getMethodName() {
        return table.getCode();
    }

    @Override
    protected TypeInfo getReturnType() {

        return null;
    }

    @Override
    protected ArrayList<Modifier.ModifierKeyword> getAccessModifiers() {
        ArrayList<Modifier.ModifierKeyword> arrayList = new ArrayList<>();
        arrayList.add(Modifier.ModifierKeyword.PUBLIC_KEYWORD);
        return arrayList;
    }

    @Override
    protected ArrayList<ParameterInfo> getParameterCollection() {
        ArrayList<ParameterInfo> parameterInfos = new ArrayList<>();

        return parameterInfos;
    }
}
