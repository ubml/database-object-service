/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.generate;

import io.iec.edp.caf.generator.baseInfo.ParameterInfo;
import io.iec.edp.caf.generator.baseInfo.TypeInfo;
import io.iec.edp.caf.generator.method.ClassMethodGenerator;
import io.iec.edp.caf.databaseobject.api.entity.DatabaseObjectTable;
import org.eclipse.jdt.core.dom.*;

import java.util.ArrayList;

/**
 * @author liu_wei
 */
public class DboClassMethodGenerator extends ClassMethodGenerator {

    private DatabaseObjectTable table;

    public DboClassMethodGenerator(DatabaseObjectTable databaseObject) {
        this.table = databaseObject;
    }


    @Override
    protected boolean buildIsConstructor() {
        return true;
    }

    @Override
    protected Block buildMethodBody() {
        Block block = ast.newBlock();
        Assignment assignment = ast.newAssignment();
        FieldAccess fieldAccess = ast.newFieldAccess();
        fieldAccess.setExpression(ast.newThisExpression());
        fieldAccess.setName(ast.newSimpleName("Id"));
        assignment.setLeftHandSide(fieldAccess);
        assignment.setRightHandSide(ast.newSimpleName("Id"));
        assignment.setOperator(Assignment.Operator.ASSIGN);
        block.statements().add(ast.newExpressionStatement(assignment));
        return block;
    }

    private void addSetContextArguments(MethodInvocation setContextInvocation, String varName) {
        MethodInvocation getContextInvocation = ast.newMethodInvocation();
        getContextInvocation.setExpression(ast.newThisExpression());
        getContextInvocation.setName(ast.newSimpleName("Id"));
        setContextInvocation.arguments().add(getContextInvocation);
    }

    @Override
    protected String getMethodName() {
        return table.getCode();
    }

    @Override
    protected TypeInfo getReturnType() {

        return null;
    }

    @Override
    protected ArrayList<Modifier.ModifierKeyword> getAccessModifiers() {
        return null;
    }

    @Override
    protected ArrayList<ParameterInfo> getParameterCollection() {
        ArrayList<ParameterInfo> parameterInfos = new ArrayList<>();

        ParameterInfo info = new ParameterInfo();
        info.setParamName("Id");
        //类型应该根据字段的类型获取Java实体中对应的类型
        info.setParamType(new TypeInfo(table.getCode() + "Id"));
        parameterInfos.add(info);

        return parameterInfos;
    }
}
