/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.generator.annotation;

import java.util.ArrayList;

import org.eclipse.jdt.core.dom.Modifier.ModifierKeyword;

import io.iec.edp.caf.generator.BaseGenerator;
import io.iec.edp.caf.generator.baseInfo.AnnotationTypeGenerateInfo;
import io.iec.edp.caf.generator.baseInfo.CompilationUnitInfo;
import io.iec.edp.caf.generator.baseInfo.TypeGeneratorInfo;
import io.iec.edp.caf.generator.item.ItemGeneratorContext;
import io.iec.edp.caf.generator.item.ItemInfoGenerator;

/**
 * @author liu_wei
 */
public abstract class AnnotationGenerator extends ItemInfoGenerator {

    protected AnnotationGeneratorContext getContext() {
        return (AnnotationGeneratorContext) super.getContext();
    }

    protected AnnotationGenerator(CompilationUnitInfo compileUnit) {
        super(compileUnit);

    }

    // region BeforeGenerate
    @Override
    protected void beforeGenerate() {

//		Context.ItemInfo.SetTransMethodName(BuildTransMethodName());
//		Context.ItemInfo.SetTransImplClassCollection(BuildTransImplClassCollection());
//		AddAllParentClass();
        super.beforeGenerate();

    }

    // #endregion
    // #region CreateItemInfoContext
    @Override
    protected ItemGeneratorContext createItemInfoContext() {
        AnnotationGeneratorContext context = createClassInfoContext();
        // ClassGenerateInfo classInfo = ClassGenerateInfo.Create();
        // context.ItemInfo = classInfo;
        return context;
    }

    @Override
    protected TypeGeneratorInfo createItemInfo() {
        return new AnnotationTypeGenerateInfo();
    }

    protected abstract AnnotationGeneratorContext createClassInfoContext();

    // #endregion
    // #region CreateChildGenerators
    @Override
    protected final ArrayList<BaseGenerator> createChildGenerators() {
        ArrayList<BaseGenerator> childGenerators = super.createChildGenerators();
        if (childGenerators == null) {
            childGenerators = new ArrayList<BaseGenerator>();
        }

        addAnnotationMemberGenrators(childGenerators);

        return childGenerators;
    }

    private void addAnnotationMemberGenrators(ArrayList<BaseGenerator> generators) {
        ArrayList<AnnotationMemberGenerator> extendClassGenrators = createClassExtendChildGenrators();
        if (extendClassGenrators != null && extendClassGenrators.size() > 0) {
            generators.addAll(extendClassGenrators);
        }
    }

    protected ArrayList<AnnotationMemberGenerator> createClassExtendChildGenrators() {
        return null;
    }
    // #endregion

    // #region AfterGenerate
    @Override
    protected void afterGenerate() {
        super.afterGenerate();
//			ExtendAfterGenerate();
        compileUnit.addType(getContext().getItemInfo().getTypeResult());
    }
    // #endregion

    @Override
    protected final void setAccessModifier() {
        getContext().getItemInfo().setModifiers(getAccessModifier());
    }

    protected abstract ArrayList<ModifierKeyword> getAccessModifier();

}
