/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.generator.compileunit;

import java.util.List;

import org.eclipse.jdt.core.dom.TypeDeclaration;

import io.iec.edp.caf.generator.BaseGeneratorContext;
import io.iec.edp.caf.generator.baseInfo.CompilationUnitInfo;

/**
 * @author liu_wei
 */
public class CompileUnitGeneratorContext extends BaseGeneratorContext {

    private CompilationUnitInfo compileUnitInfo;

    public final CompilationUnitInfo getCompilationUnitInfo() {
        return compileUnitInfo;
    }

    public final void setCompilationUnitInfo(CompilationUnitInfo value) {
        compileUnitInfo = value;
    }

//	private Assembly generatedAssembly;
//
//	public final Assembly getGeneratedAssembly() {
//		if (getCompilationUnitInfo().GetIsRef() == true) {
//			return refProjectGenContext.getGeneratedAssembly();
//		} else {
//			return generatedAssembly;
//		}
//	}

//	public final void setGeneratedAssembly(Assembly value) {
//		generatedAssembly = value;
//	}

    private CompileUnitGeneratorContext refProjectGenContext = null;

//	byte[] getAssemblyRaw()
//
//	public final void SetAssemblyInfo(String assemblyName, String nameSpace,
//			java.util.ArrayList<Assembly> refAssemblies, String targetFrame, OutputType type) {
//		getCompilationUnitInfo().AssemblyName = assemblyName;
//		getCompilationUnitInfo().SetRefAssemblies(refAssemblies);
//		getCompilationUnitInfo().SetOutputType(type);
//		getCompilationUnitInfo().TargetFrame = targetFrame;
//		// ProjectAssemblyInfo.SetNameSpace(nameSpace);
//	}


//	public final void AddRefAssembly(Assembly assembly) {
//		getCompilationUnitInfo().AddRefAssemblies(assembly);
//	}

    public final void addImport(String importName) {
        compileUnitInfo.addImport(importName);
    }

    public void setImports(List<String> imports) {
        compileUnitInfo.setImports(imports);
    }

    public void setPackageName(String packageName) {
        compileUnitInfo.setPackageName(packageName);
    }

    public String getPackageName() {
        return compileUnitInfo.getPackageName();
    }

    public void setFilePath(String filePath) {
        compileUnitInfo.setFilePath(filePath);
    }

    // TODO JAVA�ܲ����ڰ��ϴ�ע��
//	public final void AddAttribute(AttributeInfo attribute) {
//		compileUnitInfo.addAttribute(attribute);
//	}

    public final void addChildGeneratorResult(TypeDeclaration memberResult) {
        compileUnitInfo.addType(memberResult);
    }

//	public final void SetIsRef(boolean value) {
//		getCompilationUnitInfo().SetIsRef(value);
//	}
//
//	public final void SetUseReferencesBuffer(boolean value) {
//		getCompilationUnitInfo().SetReferencesBuffer(value);
//	}
//
//	public final boolean GetIsRef() {
//		return getCompilationUnitInfo().GetIsRef();
//	}
//
//	public final void SetRefProjectGen(CompileUnitGeneratorContext context) {
//		refProjectGenContext = context;
//		getCompilationUnitInfo().SetRefProjectGen(context.getCompilationUnitInfo());
//	}
}
