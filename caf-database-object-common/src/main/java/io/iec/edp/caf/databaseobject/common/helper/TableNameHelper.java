/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.common.helper;

import io.iec.edp.caf.databaseobject.api.configuration.TableNameConfiguration;
import io.iec.edp.caf.databaseobject.api.configuration.TableNameConfigurationLoader;
import io.iec.edp.caf.databaseobject.spi.ITableNameRuleManager;

import java.io.IOException;

/**
 * @author liu_wei
 */
public class TableNameHelper extends TableNameConfigurationLoader {
    private static TableNameHelper instance;

    public static TableNameHelper getInstance() throws IOException {
        return (instance != null) ? instance : (instance = new TableNameHelper());
    }

    /**
     * 构造器
     */
    private TableNameHelper() throws IOException {
        super();
    }

    /**
     * 返回各类型数据库sql生成器
     *
     * @param typeName 类型名称
     * @return 对应规则Manager
     */
    public final ITableNameRuleManager GetManager(String typeName) throws IOException {
        ITableNameRuleManager manager = null;
        TableNameConfiguration data = GetDatabaseObjectConfigurationData(typeName);
        if (data != null) {
            Class<?> cls = null;
            try {
                cls = Class.forName(data.getName());
                manager = (ITableNameRuleManager) cls.newInstance();
            } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return manager;
    }
}
