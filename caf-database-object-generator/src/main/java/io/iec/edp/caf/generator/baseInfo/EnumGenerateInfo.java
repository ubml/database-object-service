/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.generator.baseInfo;

import java.util.List;

import org.eclipse.jdt.core.dom.AbstractTypeDeclaration;
import org.eclipse.jdt.core.dom.BodyDeclaration;
import org.eclipse.jdt.core.dom.EnumDeclaration;
import org.eclipse.jdt.core.dom.Modifier.ModifierKeyword;

/**
 * @author liu_wei
 */
public class EnumGenerateInfo extends TypeGeneratorInfo {

    private List<EnumConstantInfo> enumConstants;

    public void setEnumConstants(List<EnumConstantInfo> enumConstants) {
        this.enumConstants = enumConstants;
    }

    protected EnumDeclaration enumDeclarationResult;

    @Override
    public AbstractTypeDeclaration getTypeResult() {
        return enumDeclarationResult;
    }

    @Override
    public void generate() {
        enumDeclarationResult = ast.newEnumDeclaration();
        enumDeclarationResult.bodyDeclarations().addAll(bodyDeclarations);

        enumDeclarationResult.setName(ast.newSimpleName(typeName));
        if (modifiers != null) {
            for (ModifierKeyword modifier : modifiers) {
                enumDeclarationResult.modifiers().add(ast.newModifier(modifier));
            }
        }
        if (superInterfaces != null) {
            for (TypeInfo superInterface : superInterfaces) {
//			typeDeclarationResult.superInterfaces().add(superInterface.GetType());
                enumDeclarationResult.superInterfaceTypes().add(superInterface.getType(ast));
            }
        }
        if (enumConstants != null) {
            for (EnumConstantInfo constant : enumConstants)
                enumDeclarationResult.enumConstants().add(constant.getEnumConstant(ast));
        }
        typeDeclarationResult = enumDeclarationResult;
    }

    public void addChildDeclaration(BodyDeclaration child) {
        bodyDeclarations.add(child);
    }

    @Override
    protected final Boolean getIsInterface() {
        return false;
    }
}
