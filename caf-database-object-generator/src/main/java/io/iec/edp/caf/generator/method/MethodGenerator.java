/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.generator.method;

import java.util.ArrayList;

import io.iec.edp.caf.generator.baseInfo.AnnotationInfo;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.Modifier.ModifierKeyword;

import io.iec.edp.caf.generator.BaseGenerator;
import io.iec.edp.caf.generator.BaseGeneratorContext;
import io.iec.edp.caf.generator.baseInfo.MethodGenerateInfo;
import io.iec.edp.caf.generator.baseInfo.ParameterInfo;
import io.iec.edp.caf.generator.baseInfo.TypeInfo;

/**
 * @author liu_wei
 */
public abstract class MethodGenerator extends BaseGenerator {

    protected MethodGeneratorContext getContext() {
        return (MethodGeneratorContext) super.getContext();
    }

    public MethodDeclaration getResult() {
        return getContext().getMethodGenInfo().getMethodResult();
    }

    @Override
    protected BaseGeneratorContext createContext() {
        MethodGeneratorContext context = createMethodInfoContext();
        MethodGenerateInfo methodInfo = createMethodInfo();
        methodInfo.setAst(ast);
        context.setMethodGenInfo(methodInfo);
        return context;
        // Context = new ItemGeneratorContext();
    }

    @Override
    protected void beforeGenerate() {
        ArrayList<AnnotationInfo> attrs = getAttributeList();
        getContext().getMethodGenInfo().setAttributes(attrs);
        if (attrs != null) {
            for (AnnotationInfo attr :
                    attrs) {
                String typeFullName = attr.getTypeInfo().getTypeFullName();
                if (typeFullName != null)
                    addImport(typeFullName);
            }
        }
    }

    @Override
    protected void doGenerate() {
        String methodName = getMethodName();

        TypeInfo returnType = getReturnType();
        if (returnType != null) {
            String typeFullName = returnType.getTypeFullName();
            if (typeFullName != null)
                addImport(typeFullName);
        }
        ArrayList<ModifierKeyword> accessModifiers = getAccessModifiers();
        ArrayList<ParameterInfo> parameterCollection = getParameterCollection();
        if (parameterCollection != null) {
            for (ParameterInfo param :
                    parameterCollection) {
                String paramType = param.getParamType().getTypeFullName();
                if (paramType != null)
                    addImport(paramType);
            }
        }
        getContext().initMethodInfo(methodName, returnType, accessModifiers, parameterCollection);

        getContext().getMethodGenInfo().generate();
    }

    protected abstract MethodGeneratorContext createMethodInfoContext();

    protected abstract MethodGenerateInfo createMethodInfo();

    protected abstract String getMethodName();

    protected abstract TypeInfo getReturnType();

    protected ArrayList<AnnotationInfo> getAttributeList() {
        return null;
    }

    protected abstract ArrayList<ModifierKeyword> getAccessModifiers();

    protected abstract ArrayList<ParameterInfo> getParameterCollection();
}
