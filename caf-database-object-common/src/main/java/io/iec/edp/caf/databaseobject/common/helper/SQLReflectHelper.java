/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.common.helper;

import io.iec.edp.caf.databaseobject.IDatabaseObjectSQLReflect;
import io.iec.edp.caf.databaseobject.api.configuration.DatabaseObjectConfiguration;
import io.iec.edp.caf.databaseobject.api.configuration.DatabaseObjectConfigurationLoader;

/**
 * @author liu_wei
 */
public class SQLReflectHelper extends DatabaseObjectConfigurationLoader {

    private static SQLReflectHelper instance;

    /**
     * 元数据序列化器帮助
     */

    public static SQLReflectHelper getInstance() {
        return (instance != null) ? instance : (instance = new SQLReflectHelper());
    }

    /**
     * 构造器
     */
    private SQLReflectHelper() {

    }

    /**
     * 返回各类型数据库sql生成器
     *
     * @param typeName
     * @return
     */
    public final IDatabaseObjectSQLReflect GetManager(String typeName) {
        IDatabaseObjectSQLReflect manager = null;
        DatabaseObjectConfiguration data = GetDatabaseObjectConfigurationData(typeName);
        if (data != null) {
            Class<?> cls = null;
            try {
                cls = Class.forName(data.getSQLReflector().getName());
                manager = (IDatabaseObjectSQLReflect) cls.newInstance();
            } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return manager;
    }
}
