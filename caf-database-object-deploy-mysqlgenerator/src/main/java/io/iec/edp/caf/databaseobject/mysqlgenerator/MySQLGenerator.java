/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.databaseobject.mysqlgenerator;

import io.iec.edp.caf.databaseobject.defaultsqlgenerator.AbstractDefaultSqlGenerator;

import java.util.ArrayList;
import java.util.List;

/**
 * MySQL数据库sql生成器
 *
 * @author liu_wei
 */
public class MySQLGenerator extends AbstractDefaultSqlGenerator {

    @Override
    public List<String> GetViewSql(String viewName, String defination) {
        List<String> result = new ArrayList<String>();
        String dropSql = " drop VIEW IF EXISTS " + viewName;
        result.add(dropSql);
        String createSql = String.format("Create View %1$s AS ", viewName) + defination;
        result.add(createSql);
        return result;
    }

    @Override
    public String getReNameColumnSql(String tableName, String oldColName, String newColName) {
        //统一为小写
        return "ALTER TABLE " + tableName + " RENAME COLUMN " + oldColName.toLowerCase() + " TO " + newColName.toLowerCase();
    }

    @Override
    public String getDefaultValue(String schemaName, String tableName, String colName) {
        return "SELECT COLUMN_DEFAULT as defaultvalue from information_schema.columns where column_name= '" + colName + "' AND table_name= '" + tableName + "' and TABLE_SCHEMA = '" + schemaName + "'";
    }

}
