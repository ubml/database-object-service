/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.generator.property;

import java.util.ArrayList;

import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.Modifier.ModifierKeyword;

import io.iec.edp.caf.generator.baseInfo.ClassPropertyGenerateInfo;
import io.iec.edp.caf.generator.baseInfo.PropertyGenerateInfo;
import io.iec.edp.caf.generator.item.ClassGenerator;

/**
 * @author liu_wei
 */
public abstract class ClassPropertyGenerator extends PropertyGenerator {

    protected ClassPropertyGeneratorContext getContext() {
        return (ClassPropertyGeneratorContext) super.getContext();
    }

    @Override
    protected ClassPropertyGeneratorContext createPropertyGeneratorContext() {
        return new ClassPropertyGeneratorContext();
    }

    @Override
    protected PropertyGenerateInfo createPropertyInfo() {
        return new ClassPropertyGenerateInfo();
    }

    @Override
    protected void beforeGenerate() {
        super.beforeGenerate();
        getContext().getPropertyInfo().setGetterModifiers(getGetterAccessModifier());
        getContext().getPropertyInfo().setSetterModifiers(getSetterAccessModifier());
        getContext().getPropertyInfo().setGetterBlock(buildGetMethodBody());
        getContext().getPropertyInfo().setSetterBlock(buildSetMethodBody());
        getContext().getPropertyInfo().setGetterOverride(isGetterOverride());
        getContext().getPropertyInfo().setSetterOverride(isSetterOverride());
    }

    @Override
    protected void doGenerate() {
        getContext().getPropertyInfo().generate();
    }

    @Override
    protected final void afterGenerate() {
        ArrayList<MethodDeclaration> result = getContext().getPropertyInfo().getPropResult();
        for (MethodDeclaration methodDeclaration : result) {
            ((ClassGenerator) parentGenerator).addChildSyntax(methodDeclaration);
        }

    }

    protected boolean isGetterOverride() {
        return false;
    }

    protected boolean isSetterOverride() {
        return false;
    }

    protected abstract ArrayList<ModifierKeyword> getGetterAccessModifier();

    protected abstract ArrayList<ModifierKeyword> getSetterAccessModifier();

    protected Block buildGetMethodBody() {
        return null;
    }

    protected Block buildSetMethodBody() {
        return null;
    }

}
