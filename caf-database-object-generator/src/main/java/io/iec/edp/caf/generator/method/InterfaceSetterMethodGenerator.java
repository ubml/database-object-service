/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.generator.method;

import java.util.ArrayList;

import io.iec.edp.caf.generator.baseInfo.AnnotationInfo;
import org.eclipse.jdt.core.dom.Modifier.ModifierKeyword;

import io.iec.edp.caf.generator.baseInfo.ParameterInfo;
import io.iec.edp.caf.generator.baseInfo.TypeInfo;

/**
 * @author liu_wei
 */
public class InterfaceSetterMethodGenerator extends InterfaceMethodGenerator {
    private static final String ParamName = "value";
    private String propName;
    private String fieldName;
    private TypeInfo propType;
    private ArrayList<AnnotationInfo> attributes;

    public InterfaceSetterMethodGenerator(String propName, String fieldName, TypeInfo propType,
                                          ArrayList<AnnotationInfo> attributes) {
        this.propName = propName;
        this.fieldName = fieldName;
        this.propType = propType;
        this.attributes = attributes;
    }

    @Override
    protected String getMethodName() {

        return "set" + propName;
    }

    @Override
    protected TypeInfo getReturnType() {
        return new TypeInfo("void");
    }

    @Override
    protected ArrayList<ModifierKeyword> getAccessModifiers() {
        return null;
    }

    @Override
    protected ArrayList<ParameterInfo> getParameterCollection() {
        ParameterInfo param = new ParameterInfo();
        param.setParamName(ParamName);
        param.setParamType(propType);
        ArrayList<ParameterInfo> params = new ArrayList<ParameterInfo>();
        params.add(param);
        return params;
    }

    @Override
    protected ArrayList<AnnotationInfo> getAttributeList() {
        return attributes;
    }
}
