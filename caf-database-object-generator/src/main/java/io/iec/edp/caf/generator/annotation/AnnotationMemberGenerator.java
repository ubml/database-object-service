/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.generator.annotation;

import java.util.ArrayList;

import io.iec.edp.caf.generator.baseInfo.AnnotationMemberInfo;
import org.eclipse.jdt.core.dom.Modifier.ModifierKeyword;

import io.iec.edp.caf.generator.BaseGenerator;
import io.iec.edp.caf.generator.BaseGeneratorContext;
import io.iec.edp.caf.generator.baseInfo.TypeInfo;

/**
 * @author liu_wei
 */
public abstract class AnnotationMemberGenerator extends BaseGenerator {

    protected AnnotationMemberGenContext getContext() {
        return (AnnotationMemberGenContext) super.getContext();
    }

    @Override
    protected BaseGeneratorContext createContext() {
        AnnotationMemberGenContext context = createAnnotationMemberContext();
        AnnotationMemberInfo memberInfo = createMemberInfo();
        memberInfo.setAst(ast);
        context.setMemberGenInfo(memberInfo);
        return context;
        // Context = new ItemGeneratorContext();
    }

    @Override
    protected void doGenerate() {
        String name = getName();
//		DataValidator.CheckForBlankString(methodName, "MethodName");
        TypeInfo type = getType();
        String typeFullName = type.getTypeFullName();
        if (typeFullName != null)
            addImport(typeFullName);

        ArrayList<ModifierKeyword> accessModifiers = getAccessModifiers();
        getContext().initMemberInfo(name, type, accessModifiers);
//		getContext().getMethodGenInfo().SetGeneratedrRetrurnType(GetGeneratedReturnType());

        getContext().getMemberGenInfo().generate();
    }

    protected abstract AnnotationMemberGenContext createAnnotationMemberContext();

    protected abstract AnnotationMemberInfo createMemberInfo();

    protected abstract String getName();

    protected abstract TypeInfo getType();

    protected abstract ArrayList<ModifierKeyword> getAccessModifiers();

}
