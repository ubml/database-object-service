/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.generator.property;

import java.util.ArrayList;

import org.eclipse.jdt.core.dom.MethodDeclaration;

import io.iec.edp.caf.generator.baseInfo.InterfacePropertyGenerateInfo;
import io.iec.edp.caf.generator.item.InterfaceGenerator;

/**
 * @author liu_wei
 */
public abstract class InterfacePropertyGenerator extends PropertyGenerator {

    protected InterfacePropertyGeneratorContext getContext() {
        return (InterfacePropertyGeneratorContext) super.getContext();
    }

    @Override
    protected InterfacePropertyGeneratorContext createPropertyGeneratorContext() {
        return new InterfacePropertyGeneratorContext();
    }

    @Override
    protected InterfacePropertyGenerateInfo createPropertyInfo() {
        return new InterfacePropertyGenerateInfo();
    }

    @Override
    protected final void afterGenerate() {
        ArrayList<MethodDeclaration> result = getContext().getPropertyInfo().getPropResult();
        for (MethodDeclaration methodDeclaration : result) {
            ((InterfaceGenerator) parentGenerator).addChildSyntax(methodDeclaration);
        }

    }
}
