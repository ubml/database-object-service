/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.generator.baseInfo;

import java.util.ArrayList;

import org.eclipse.jdt.core.dom.MethodDeclaration;

/**
 * @author liu_wei
 */
public abstract class PropertyGenerateInfo extends BaseGenerateInfo {
    protected String propertyName;
    protected String fieldName;
    protected TypeInfo propertyType;

    protected ArrayList<AnnotationInfo> gettrtAttributes;
    protected ArrayList<AnnotationInfo> settrtAttributes;

    protected boolean hasGetMethod = true;
    protected boolean hasSetMethod = false;

    protected ArrayList<MethodDeclaration> propResult;

    public void setGettrtAttributes(ArrayList<AnnotationInfo> gettrtAttributes) {
        this.gettrtAttributes = gettrtAttributes;
    }

    public void setSettrtAttributes(ArrayList<AnnotationInfo> settrtAttributes) {
        this.settrtAttributes = settrtAttributes;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public void setPropertyType(TypeInfo propertyType) {
        this.propertyType = propertyType;
    }

    public void setHasGetMethod(boolean hasGetMethod) {
        this.hasGetMethod = hasGetMethod;
    }

    public void setHasSetMethod(boolean hasSetMethod) {
        this.hasSetMethod = hasSetMethod;
    }

    public ArrayList<MethodDeclaration> getPropResult() {
        return propResult;
    }

    @Override
    public void generate() {

    }
}
