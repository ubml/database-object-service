/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.generator.baseInfo;

import java.util.ArrayList;

import org.eclipse.jdt.core.dom.MethodDeclaration;

import io.iec.edp.caf.generator.method.InterfaceGetterMethodGenerator;
import io.iec.edp.caf.generator.method.InterfaceSetterMethodGenerator;

/**
 * @author liu_wei
 */
public class InterfacePropertyGenerateInfo extends PropertyGenerateInfo {

    @Override
    public void generate() {
        propResult = new ArrayList<MethodDeclaration>();
        if (hasGetMethod)
            propResult.add(GetGetterDeclaration());
        if (hasSetMethod)
            propResult.add(SetGetterDeclaration());

    }

    private MethodDeclaration GetGetterDeclaration() {
        InterfaceGetterMethodGenerator getterGenerator = new InterfaceGetterMethodGenerator(propertyName, fieldName,
                propertyType, gettrtAttributes);
        getterGenerator.setAst(ast);
        getterGenerator.initialize();
        getterGenerator.generate();
        return getterGenerator.getResult();
    }

    private MethodDeclaration SetGetterDeclaration() {
        InterfaceSetterMethodGenerator setterGenerator = new InterfaceSetterMethodGenerator(propertyName, fieldName,
                propertyType, settrtAttributes);
        setterGenerator.setAst(ast);
        setterGenerator.initialize();
        setterGenerator.generate();
        return setterGenerator.getResult();
    }
}
