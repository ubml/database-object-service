package io.iec.edp.caf.databaseobject.rtmanager.repository;

import io.iec.edp.caf.data.orm.DataRepository;
import io.iec.edp.caf.databaseobject.api.entity.GspDatabaseObjectMapping;

import java.util.List;

public interface GspDatabaseObjectMappingRepository extends DataRepository<GspDatabaseObjectMapping, String> {
    GspDatabaseObjectMapping findByDboId(String var1);
    List<GspDatabaseObjectMapping> findAllByDboIdIn(List<String> ids);
}
