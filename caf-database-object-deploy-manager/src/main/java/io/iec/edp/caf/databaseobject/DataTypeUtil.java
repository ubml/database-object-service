/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject;

import io.iec.edp.caf.generator.baseInfo.TypeInfo;
import io.iec.edp.caf.databaseobject.api.entity.DataType;

import java.sql.Clob;
import java.util.Date;

/**
 * @author liu_wei
 */
public class DataTypeUtil {
    public TypeInfo getTypeInfoByDataType(DataType type) {
        switch (type) {
            case LongInt:
            case Int:
            case SmallInt:
                return new TypeInfo(Integer.class.getTypeName());
            case Varchar:
            case NVarchar:
                return new TypeInfo(String.class.getTypeName());
            case Char:
            case NChar:
                return new TypeInfo(Character.class.getTypeName());
            case Clob:
            case NClob:
                return new TypeInfo(Clob.class.getTypeName());
            case Boolean:
                return new TypeInfo(boolean.class.getTypeName());
            case Blob:
                return new TypeInfo(Byte.class.getTypeName());
            case Decimal:
                return new TypeInfo(Float.class.getTypeName());
            case DateTime:
                return new TypeInfo(Date.class.getTypeName());
            case TimeStamp:
                return new TypeInfo(Date.class.getTypeName());
            default:
                return new TypeInfo(String.class.getTypeName());
        }
    }
}
