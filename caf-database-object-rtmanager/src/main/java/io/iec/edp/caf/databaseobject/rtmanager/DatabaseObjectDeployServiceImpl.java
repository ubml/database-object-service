/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.rtmanager;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.databaseobject.DboDeployManager;
import io.iec.edp.caf.databaseobject.api.entity.AbstractDatabaseObject;
import io.iec.edp.caf.databaseobject.api.entity.DBInfo;
import io.iec.edp.caf.databaseobject.api.entity.DatabaseObjectTable;
import io.iec.edp.caf.databaseobject.api.entity.DboDeployResultInfo;
import io.iec.edp.caf.databaseobject.api.service.IDatabaseObjectDeployService;
import io.iec.edp.caf.databaseobject.api.service.IDatabaseObjectRtService;
import io.iec.edp.caf.databaseobject.common.DatabaseObjectCommonUtil;
import io.iec.edp.caf.databaseobject.manager.DatabaseObjectServiceImpl;
import io.iec.edp.caf.rpc.api.service.RpcClient;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

/**
 * @author liu_wei
 */
@Slf4j
public class DatabaseObjectDeployServiceImpl implements IDatabaseObjectDeployService {

    private RpcClient client = SpringBeanUtils.getBean(RpcClient.class);

    @Override
    public DboDeployResultInfo deployDatabaseObjects(List<AbstractDatabaseObject> databaseObjcts, Map<String, String> dimensionValue) {
        DboDeployManager dboDeployManager = new DboDeployManager();
        DatabaseObjectServiceImpl service = new DatabaseObjectServiceImpl();
        DBInfo dbInfo = service.getDbInfo();
        return dboDeployManager.deployDboList(databaseObjcts, dimensionValue, dbInfo);
    }

    @Override
    public DboDeployResultInfo deployDatabaseObjects(List<AbstractDatabaseObject> databaseObjects, Map<String, String> dimensionValue, String su) {
        try {
            LinkedHashMap<String, Object> parameters = new LinkedHashMap<String, Object>();
            ObjectMapper mapper = new ObjectMapper();
            mapper.setPropertyNamingStrategy(PropertyNamingStrategy.UPPER_CAMEL_CASE);
            String content = mapper.writeValueAsString(databaseObjects);
            parameters.put("databaseObjects", content);
            parameters.put("dimensionValue", dimensionValue);
            return client.invoke(DboDeployResultInfo.class, "io.iec.edp.caf.databaseobject.rpcapi.service.DatabaseObjectRpcService.deployDatabaseObjects", su, parameters, null);
        } catch (Exception e) {
            throw new RuntimeException("部署DBO报错：" + e.getMessage(), e);
        }

    }

    @Override
    public DboDeployResultInfo deployDatabaseObjects(List<AbstractDatabaseObject> databaseObjects, Map<String, String> dimensionValue, DBInfo dbInfo) {
        DboDeployManager dboDeployManager = new DboDeployManager();
        return dboDeployManager.deployDboList(databaseObjects, dimensionValue, dbInfo);
    }

    @Override
    public void modifyI18nDatabaseObjectsByNewLanguage(List<String> languages) {
        IDatabaseObjectRtService databaseObjectRtService = SpringBeanUtils.getBean(IDatabaseObjectRtService.class);
        List<DatabaseObjectTable> tables = databaseObjectRtService.getI18nObjectTables();
        if (tables == null || tables.size() == 0) {
            return;
        }
        DatabaseObjectCommonUtil databaseObjectCommonUtil = new DatabaseObjectCommonUtil();
        DBInfo dbInfo = databaseObjectCommonUtil.getCurrentDbInfo();
        Map<String, List<String>> dimensionInfo = new HashMap<>();
        //存在带维度的DBO，需要获取维度值,否则维度值为空
        boolean isNeedDimension = false;
        for (DatabaseObjectTable table : tables) {
            if (table.getHasNameRule()) {
                isNeedDimension = true;
                break;
            }
        }
        if (isNeedDimension) {
            dimensionInfo = databaseObjectCommonUtil.getDefaultDimensionInfo(dbInfo);
        }
        List<AbstractDatabaseObject> abstractDatabaseObjects = new ArrayList<>();
        for (DatabaseObjectTable table : tables) {
            abstractDatabaseObjects.add((AbstractDatabaseObject) table);
        }
        DatabaseObjectCommonUtil.addLanguages(languages);
        DboDeployManager dboDeployManager = new DboDeployManager();
        dboDeployManager.deployDboWithDimensionList(abstractDatabaseObjects, dimensionInfo, dbInfo);
    }

    @Override
    public void deployDboList(String dboPath, Map<String, String> dimensionInfo) {
        DboDeployManager dboDeployManager = new DboDeployManager();
        DatabaseObjectServiceImpl service = new DatabaseObjectServiceImpl();
        DBInfo dbInfo = service.getDbInfo();
        dboDeployManager.deployDboList(dboPath, dimensionInfo, dbInfo);
    }

    @Override
    public void deployDboWithDimensionList(String dboPath, Map<String, List<String>> dimensionInfos) {
        DboDeployManager dboDeployManager = new DboDeployManager();
        DatabaseObjectServiceImpl service = new DatabaseObjectServiceImpl();
        DBInfo dbInfo = service.getDbInfo();
        dboDeployManager.deployDboWithDimensionList(dboPath, dimensionInfos, dbInfo);
    }

    @Override
    public void deployDboList(String dboPath, Map<String, String> dimensionInfo, DBInfo dbInfo) {
        DboDeployManager dboDeployManager = new DboDeployManager();
        dboDeployManager.deployDboList(dboPath, dimensionInfo, dbInfo);
    }

    @Override
    public void deployDboWithDimensionList(String dboPath, Map<String, List<String>> dimensionInfos, DBInfo dbInfo) {
        DboDeployManager dboDeployManager = new DboDeployManager();
        dboDeployManager.deployDboWithDimensionList(dboPath, dimensionInfos, dbInfo);
    }

    @Override
    public void deployDatabaseObject(AbstractDatabaseObject databaseObject, Map<String, String> dimensionInfo) {
        if (databaseObject == null) {
            throw new RuntimeException("部署的数据库对象不能为null！");
        }
        DboDeployManager dboDeployManager = new DboDeployManager();
        DatabaseObjectServiceImpl service = new DatabaseObjectServiceImpl();
        DBInfo dbInfo = service.getDbInfo();
        dboDeployManager.deployDbo(databaseObject, dimensionInfo, dbInfo);
    }

    @Override
    public void deployDatabaseObject(AbstractDatabaseObject databaseObject, Map<String, String> dimensionInfo, String su) {
        try {
            LinkedHashMap<String, Object> parameters = new LinkedHashMap<String, Object>();
            DatabaseObjectCommonUtil commonUtil = new DatabaseObjectCommonUtil();
            String object = commonUtil.serialze(databaseObject);
            parameters.put("databaseObject", object);
            parameters.put("dimensionInfo", dimensionInfo);
            client.invoke(String.class, "io.iec.edp.caf.databaseobject.rpcapi.service.DatabaseObjectRpcService.deployDatabaseObject", su, parameters, null);
        } catch (Exception e) {
            throw new RuntimeException("部署DBO报错：" + e.getMessage(), e);
        }
    }
}
