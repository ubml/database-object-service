/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.generator.baseInfo;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.Modifier.ModifierKeyword;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

/**
 * @author zhouyjtr
 */
public class FieldGenerateInfo extends BaseGenerateInfo {

    private String fieldName;

    private TypeInfo filedType;

    private List<ModifierKeyword> modifiers;

    private Expression initializer;

    private List<AnnotationInfo> attributes;

    private FieldDeclaration filedResult;

    public FieldDeclaration getFiledResult() {
        return filedResult;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public void setFieldType(TypeInfo filedType) {
        this.filedType = filedType;
    }

    public void setInitializer(Expression initializer) {
        this.initializer = initializer;
    }

    public void setModifiers(List<ModifierKeyword> modifiers) {
        this.modifiers = modifiers;
    }

    public void setAttributes(List<AnnotationInfo> attributes) {
        this.attributes = attributes;
    }

    public static FieldGenerateInfo create() {
        return new FieldGenerateInfo();
    }

    private FieldGenerateInfo() {
        modifiers = new ArrayList<ModifierKeyword>();
        attributes = new ArrayList<AnnotationInfo>();
    }

    // TODO
    // ��ʱû�кõķ�ʽ��internal�����ڵ�ǰ��������java��ʽ���ֺ�һ�������µ����������ò��ˣ�����C#����Щ��ʵ����ֻ��һ�������µĲ�ͬ�����ռ�
    @Override
    public void generate() {

        VariableDeclarationFragment varFreg = ast.newVariableDeclarationFragment();
        SimpleName varName = ast.newSimpleName(fieldName);

        varFreg.setName(varName);
//		StringLiteral varInitializer = ast.newStringLiteral();
//		varInitializer.setLiteralValue("Hello Excel");
//		varFreg.setInitializer(varInitializer);

        varFreg.setInitializer(initializer);

        filedResult = ast.newFieldDeclaration(varFreg);
        if (modifiers != null) {
            for (ModifierKeyword modifierKeyword : modifiers) {
                filedResult.modifiers().add(ast.newModifier(modifierKeyword));
            }
        }
        if (attributes != null) {
            for (AnnotationInfo attributeInfo : attributes) {
                filedResult.modifiers().add(attributeInfo.getAnnotation(ast));
            }
        }
        Type type = filedType.getType(ast);
        filedResult.setType(type);

    }

}
