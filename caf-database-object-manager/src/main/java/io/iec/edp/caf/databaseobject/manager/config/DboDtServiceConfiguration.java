/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.manager.config;

import io.iec.edp.caf.databaseobject.api.service.IDatabaseObjectExportService;
import io.iec.edp.caf.databaseobject.api.service.IDatabaseObjectService;
import io.iec.edp.caf.databaseobject.manager.DatabaseObjectServiceImpl;
import io.iec.edp.caf.databaseobject.manager.export.DatabaseObjectExportServiceImpl;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author zhaoleitr
 */
@Configuration
@EntityScan("io.iec.edp.caf.databaseobject.api.entity")
public class DboDtServiceConfiguration {

    @Bean
    public IDatabaseObjectService databaseObjectDtService() {

        return new DatabaseObjectServiceImpl();
    }

    @Bean
    public IDatabaseObjectExportService databaseObjectExportService() {
        return new DatabaseObjectExportServiceImpl();
    }
}
