/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.manager.persistent;


import io.iec.edp.caf.databaseobject.api.context.DatabaseObjectContext;
import io.iec.edp.caf.databaseobject.api.entity.AbstractDatabaseObject;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author liu_wei
 */
public class DatabaseObjectManagerRepository implements IDatabaseObjectManagerRepository {
    private DatabaseObjectDac dac = new DatabaseObjectDac();

    @Override
    public void createDatabaseObject(String fullPath, AbstractDatabaseObject object) {
        dac.createDatabaseObject(fullPath, object);
    }

    @Override
    public void updateDatabaseObject(String fullPath, AbstractDatabaseObject object) {
        dac.updateDatabaseObject(fullPath, object);
    }

    @Override
    public final boolean isFileExist(String fullPath) {
        return (new File(fullPath)).isFile();
    }

    @Override
    public final String read(String fullPath) {
        return dac.read(fullPath);
    }

    @Override
    public final ArrayList<String> getFilePaths(String path) {
        return dac.getFilePaths(path);
    }

    @Override
    public final void createFolder(String path) {
        (new File(path)).mkdirs();
    }

    @Override
    public final boolean isDirectoryExist(String path) {
        return (new File(path)).isDirectory();
    }

    public final ArrayList<String> getFilePathsRecursivly(String path) {
        //递归查找当前路径下dbo文件，记录所有dbo文件所在全路径
        ArrayList<String> dbofilePath = new ArrayList<String>();
        getDboFilePathRecursivly(new File(path), dbofilePath);

        if (dbofilePath.size() < 1) {
            return null;
        } else {
            return dbofilePath;
        }
    }

    private void getDboFilePathRecursivly(File file, ArrayList<String> dbofilePath) {
        if (!file.isDirectory()) {
            return;
        }
        if ((file.getName().equals("node_modules"))) {
            return;
        }
        File[] subFiles = file.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.isDirectory() || pathname.getName().endsWith(DatabaseObjectContext.getSuffix());
            }
        });

        if (subFiles.length > 0) {
            //先排序，兼容安装盘DBO执行顺序问题
            Arrays.sort(subFiles);
            for (File subFile : subFiles) {
                if (subFile.isFile()) {
                    if (subFile.getName().endsWith(DatabaseObjectContext.getSuffix())) {
                        dbofilePath.add(subFile.getPath());
                    }

                } else if (subFile.isDirectory()) {
                    getDboFilePathRecursivly(subFile, dbofilePath);
                }
            }

        }
    }
}
