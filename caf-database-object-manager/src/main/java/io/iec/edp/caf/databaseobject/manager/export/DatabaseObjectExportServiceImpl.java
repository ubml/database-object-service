/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.manager.export;

import io.iec.edp.caf.databaseobject.api.entity.AbstractDatabaseObject;
import io.iec.edp.caf.databaseobject.api.entity.DBInfo;
import io.iec.edp.caf.databaseobject.api.service.IDatabaseObjectExportService;
import io.iec.edp.caf.databaseobject.common.DatabaseObjectCommonUtil;
import io.iec.edp.caf.databaseobject.api.configuration.FileUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liu_wei
 */
public class DatabaseObjectExportServiceImpl implements IDatabaseObjectExportService {
    @Override
    public boolean isExportOpen() {
        return FileUtils.isExportOpen();
    }

    @Override
    public List<String> getAllTablesName(DBInfo info) {

        DatabaseObjectCommonUtil commonUtil = new DatabaseObjectCommonUtil();
        return commonUtil.getDbTables(info);
    }

    @Override
    public List<AbstractDatabaseObject> getDatabaseObjectsByTablesName(List<String> tablesName, String businessObjectId, DBInfo info) {
        DatabaseObjectExportManager manager = new DatabaseObjectExportManager(info);
        return manager.getDatabaseObjectsByTablesName(tablesName, null, businessObjectId);
    }

    @Override
    public AbstractDatabaseObject getDatabaseObjectByTableName(String tableName, String dboName, String businessObjectId, DBInfo info) {
        List<String> tablesName = new ArrayList<>();
        tablesName.add(tableName);
        DatabaseObjectExportManager manager = new DatabaseObjectExportManager(info);
        return manager.getDatabaseObjectsByTablesName(tablesName, dboName, businessObjectId).get(0);
    }
}
