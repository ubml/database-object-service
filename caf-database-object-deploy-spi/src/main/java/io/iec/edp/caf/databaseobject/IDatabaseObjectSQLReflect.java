/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject;

import java.util.List;
import java.util.Map;

/**
 * @author liu_wei
 */
public interface IDatabaseObjectSQLReflect {

    /**
     * 获取视图sql
     *
     * @param viewName       视图名称
     * @param viewDefination 视图定义
     * @return 视图SQL列表
     */
    List<String> GetViewSql(String viewName, String viewDefination);

    /**
     * 获取修改列SQL
     *
     * @param tableName  表名
     * @param oldColName 原列名
     * @param newColName 新列名
     * @return SQL语句
     */
    String getReNameColumnSql(String tableName, String oldColName, String newColName);

    /**
     * 获取删除列SQL
     *
     * @param tableName 表名
     * @param colName   列名c
     * @return SQL语句
     */
    String getDropColumnSql(String tableName, String colName);

    /**
     * 获取默认值
     *
     * @param schemaName schema
     * @param tableName  表
     * @param colName    列
     * @return 默认值
     */
    String getDefaultValue(String schemaName, String tableName, String colName);
}
