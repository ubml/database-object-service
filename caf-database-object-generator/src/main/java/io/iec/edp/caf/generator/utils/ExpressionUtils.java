/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.generator.utils;

import java.util.function.Supplier;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.CastExpression;
import org.eclipse.jdt.core.dom.ConditionalExpression;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.InstanceofExpression;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.QualifiedName;
import org.eclipse.jdt.core.dom.ReturnStatement;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.StringLiteral;
import org.eclipse.jdt.core.dom.Type;

/**
 * @author liu_wei
 */
public class ExpressionUtils {

    public static QualifiedName getQualifiedName(AST ast, String firstName, String lastName) {
        SimpleName firstSimpleName = ast.newSimpleName(firstName);
        SimpleName lastSimpleName = ast.newSimpleName(lastName);
        return ast.newQualifiedName(firstSimpleName, lastSimpleName);
    }

    public static ReturnStatement getReturnNullStatement(AST ast) {
        ReturnStatement returnStatement = ast.newReturnStatement();
        returnStatement.setExpression(ast.newNullLiteral());
        return returnStatement;
    }

    public static StringLiteral getStringLiteral(AST ast, String value) {
        StringLiteral literal = ast.newStringLiteral();
        literal.setLiteralValue(value);
        return literal;
    }

    public static MethodInvocation getMethodInvocation(AST ast, String param, String method) {
        MethodInvocation invocation = ast.newMethodInvocation();
        invocation.setExpression(ast.newSimpleName(param));
        invocation.setName(ast.newSimpleName(method));
        return invocation;
    }

    public static MethodInvocation getMethodInvocation(AST ast, MethodInvocation param,
                                                       String method) {
        MethodInvocation invocation = ast.newMethodInvocation();
        invocation.setExpression(param);
        invocation.setName(ast.newSimpleName(method));
        return invocation;
    }

    public static ConditionalExpression getAsExpression(AST ast,
                                                        java.util.function.Function<AST, Expression> valueProvider,
                                                        java.util.function.Function<AST, Type> typeProvider) {
//		String aa = value instanceof String ? (String)value : null;
        ConditionalExpression result = ast.newConditionalExpression();

        InstanceofExpression instanceofExpression = ast.newInstanceofExpression();
        instanceofExpression.setLeftOperand(valueProvider.apply(ast));
        instanceofExpression.setRightOperand(typeProvider.apply(ast));
        result.setExpression(instanceofExpression);

        CastExpression castExpression = ast.newCastExpression();
        castExpression.setType(typeProvider.apply(ast));
        castExpression.setExpression(valueProvider.apply(ast));
        result.setThenExpression(castExpression);

        result.setElseExpression(ast.newNullLiteral());

        return result;
    }
}
