/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.common.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author liu_wei
 */
public class SQLServerDatabase extends AbstractDatabaseImpl {
    public SQLServerDatabase(DbConfigData configData) {
        super(configData);
    }

    @Override
    public Connection getConnection(DbConfigData configData) throws SQLException {
        Connection c = null;
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            c = DriverManager.getConnection(configData.getConnectionString(), configData.getUserId(), configData.getPassword());
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("构造SqlServer数据库连接出错，找不到Driver:" + e);
        }
        return c;
    }

}
