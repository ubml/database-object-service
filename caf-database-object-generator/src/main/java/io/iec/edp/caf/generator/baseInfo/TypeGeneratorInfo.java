/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.generator.baseInfo;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.AbstractTypeDeclaration;
import org.eclipse.jdt.core.dom.BodyDeclaration;
import org.eclipse.jdt.core.dom.Modifier.ModifierKeyword;
import org.eclipse.jdt.core.dom.TypeDeclaration;

/**
 * @author liu_wei
 */
public abstract class TypeGeneratorInfo extends BaseGenerateInfo {

    protected String typeName;

    protected List<ModifierKeyword> modifiers;

    protected List<BodyDeclaration> bodyDeclarations;

    protected List<TypeInfo> superInterfaces;

    protected List<AnnotationInfo> attributes;

    protected AbstractTypeDeclaration typeDeclarationResult;

    public AbstractTypeDeclaration getTypeResult() {
        return typeDeclarationResult;
    }

    public void setModifiers(List<ModifierKeyword> modifiers) {
        this.modifiers = modifiers;
    }

    protected abstract Boolean getIsInterface();

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public void setAttributes(List<AnnotationInfo> attributes) {
        this.attributes = attributes;
    }

    public TypeGeneratorInfo() {
        super();
        this.modifiers = new ArrayList<ModifierKeyword>();
        this.bodyDeclarations = new ArrayList<BodyDeclaration>();
        this.superInterfaces = new ArrayList<TypeInfo>();
        this.attributes = new ArrayList<AnnotationInfo>();
    }

    void addModifier(ModifierKeyword keyword) {
        if (!modifiers.contains(keyword))
            modifiers.add(keyword);
    }

    public void addChildDeclaration(BodyDeclaration child) {
        bodyDeclarations.add(child);
    }

    public void addSuperInterface(TypeInfo interfaceType) {
        if (!superInterfaces.contains(interfaceType))
            superInterfaces.add(interfaceType);
    }

    public void addAttribute(AnnotationInfo attribute) {
        attributes.add(attribute);
    }

    @Override
    public void generate() {
        TypeDeclaration typeDeclaration = ast.newTypeDeclaration();
        typeDeclaration.bodyDeclarations().addAll(bodyDeclarations);
        typeDeclaration.setInterface(getIsInterface());
        typeDeclaration.setName(ast.newSimpleName(typeName));
        if (attributes != null) {
            for (AnnotationInfo attributeInfo : attributes) {
                typeDeclaration.modifiers().add(attributeInfo.getAnnotation(ast));

            }
        }
        if (modifiers != null) {
            for (ModifierKeyword modifier : modifiers) {
                typeDeclaration.modifiers().add(ast.newModifier(modifier));
            }
        }
        if (superInterfaces != null) {
            for (TypeInfo superInterface : superInterfaces) {
//			typeDeclarationResult.superInterfaces().add(superInterface.GetType());
                typeDeclaration.superInterfaceTypes().add(superInterface.getType(ast));
            }
        }
        typeDeclarationResult = typeDeclaration;
    }

}
