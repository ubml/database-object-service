/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.generator.method;

import java.util.ArrayList;

import org.eclipse.jdt.core.dom.Modifier.ModifierKeyword;

import io.iec.edp.caf.generator.BaseGeneratorContext;
import io.iec.edp.caf.generator.baseInfo.MethodGenerateInfo;
import io.iec.edp.caf.generator.baseInfo.ParameterInfo;
import io.iec.edp.caf.generator.baseInfo.TypeInfo;

/**
 * @author liu_wei
 */
public class MethodGeneratorContext extends BaseGeneratorContext {
    private MethodGenerateInfo methodGenInfo;

    MethodGenerateInfo getMethodGenInfo() {
        return methodGenInfo;
    }

    public void setMethodGenInfo(MethodGenerateInfo methodGenInfo) {
        this.methodGenInfo = methodGenInfo;
    }

    public final void initMethodInfo(String methodName, TypeInfo returnType, ArrayList<ModifierKeyword> accessModifiers,
                                     ArrayList<ParameterInfo> parameterCollection) {
        methodGenInfo.setMethodName(methodName);
        methodGenInfo.setReturnType(returnType);
        methodGenInfo.setParams(parameterCollection);
        methodGenInfo.setModifiers(accessModifiers);
    }
}
