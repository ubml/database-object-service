/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.generator.baseInfo;

import org.eclipse.jdt.core.dom.AST;

/**
 * @author liu_wei
 */
public abstract class BaseGenerateInfo {

    abstract void generate();

    protected AST ast = AST.newAST(AST.JLS3);

    public void setAst(AST value) {
        ast = value;
    }
}
