/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.rtmanager.repository;

import io.iec.edp.caf.data.orm.DataRepository;
import io.iec.edp.caf.databaseobject.api.entity.GspDatabaseObjectDT;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author liu_wei
 */
public interface GspDatabaseObjectDtRepository extends DataRepository<GspDatabaseObjectDT, String> {

    /**
     * 根据编号获取DBO
     *
     * @param code 编号
     * @return DBO
     */
    GspDatabaseObjectDT findByCode(String code);

    /**
     * 根据Id删除DBO
     *
     * @param id id
     */
    @Transactional(rollbackFor = Exception.class)
    void deleteGspDatabaseObjectDtById(String id);
}
