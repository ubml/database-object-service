/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.generator.method;

import java.util.ArrayList;

import io.iec.edp.caf.generator.baseInfo.AnnotationInfo;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.Modifier.ModifierKeyword;
import org.eclipse.jdt.core.dom.ReturnStatement;

import io.iec.edp.caf.generator.baseInfo.ParameterInfo;
import io.iec.edp.caf.generator.baseInfo.TypeInfo;

/**
 * @author liu_wei
 */
public class GetterMethodGenerator extends ClassMethodGenerator {

    private String propName;
    private String fieldName;
    private TypeInfo propType;
    private ArrayList<ModifierKeyword> modifiers;
    private ArrayList<AnnotationInfo> attributes;
    private Block getterBlock;
    private boolean isOverride;

    public GetterMethodGenerator(String propName, String fieldName, TypeInfo propType,
                                 ArrayList<ModifierKeyword> modifiers, ArrayList<AnnotationInfo> attributes, boolean isOverride,
                                 Block getterBlock) {
        this.propName = propName;
        this.fieldName = fieldName;
        this.propType = propType;
        this.modifiers = modifiers;
        this.attributes = attributes;
        this.getterBlock = getterBlock;
        this.isOverride = isOverride;
    }

    @Override
    protected Block buildMethodBody() {
        if (getterBlock != null)
            return getterBlock;
        getterBlock = ast.newBlock();
        ReturnStatement returnStatement = ast.newReturnStatement();
        returnStatement.setExpression(ast.newSimpleName(fieldName));
        getterBlock.statements().add(returnStatement);
        return getterBlock;
    }

    @Override
    protected String getMethodName() {

        return "get" + propName;
    }

    @Override
    protected TypeInfo getReturnType() {
        return propType;
    }

    @Override
    protected ArrayList<ModifierKeyword> getAccessModifiers() {
        return modifiers;
    }

    @Override
    protected ArrayList<ParameterInfo> getParameterCollection() {
        return null;
    }

    @Override
    protected ArrayList<AnnotationInfo> getAttributeList() {
        return attributes;
    }

    @Override
    protected boolean getIsOverride() {

        return isOverride;
    }
}
