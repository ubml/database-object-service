/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.generate;

import io.iec.edp.caf.databaseobject.api.configuration.FileUtils;
import io.iec.edp.caf.generator.baseInfo.AnnotationInfo;
import io.iec.edp.caf.generator.baseInfo.AnnotationType;
import io.iec.edp.caf.generator.baseInfo.TypeInfo;
import io.iec.edp.caf.generator.field.FieldGenerator;
import io.iec.edp.caf.databaseobject.DataTypeConverter;
import io.iec.edp.caf.databaseobject.DataTypeUtil;
import io.iec.edp.caf.databaseobject.DatabaseFuncConverter;
import io.iec.edp.caf.databaseobject.api.entity.DbType;
import io.iec.edp.caf.databaseobject.api.entity.DataType;
import io.iec.edp.caf.databaseobject.api.entity.DatabaseObjectColumn;
import io.iec.edp.caf.databaseobject.api.entity.DatabaseObjectTable;
import org.eclipse.jdt.core.dom.Modifier;
import org.eclipse.jdt.core.dom.StringLiteral;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author liu_wei
 */
public class DboFieldGenerator extends FieldGenerator {

    DatabaseObjectColumn column;
    DatabaseObjectTable table;
    private DbType dbType;

    public DbType getDbType() {
        return dbType;
    }

    public void setDbType(DbType dbType) {
        this.dbType = dbType;
    }

    public DboFieldGenerator() {

    }

    public DboFieldGenerator(DatabaseObjectColumn column, DatabaseObjectTable table, DbType dbType) {
        this.column = column;
        this.table = table;
        this.dbType = dbType;
    }

    @Override
    protected TypeInfo getFieldType() {
        if (table.getPrimaryKey() != null && table.getPrimaryKey().size() > 1 && table.getPrimaryKey().contains(column.getId())) {

            return new TypeInfo(table.getCode() + "Id");
        }

        DataTypeUtil util = new DataTypeUtil();
        return util.getTypeInfoByDataType(column.getDataType());
    }

    @Override
    protected String getFieldName() {
        String field;
        if (table.getPrimaryKey() != null && table.getPrimaryKey().size() > 1 && table.getPrimaryKey().contains(column.getId())) {
            field = "Id";
        } else {
            field = this.column.getCode().toUpperCase();
        }
        return field;
    }

    @Override
    protected ArrayList<Modifier.ModifierKeyword> getAccessModifier() {
        ArrayList<Modifier.ModifierKeyword> arrayList = new ArrayList<>();
        arrayList.add(Modifier.ModifierKeyword.PUBLIC_KEYWORD);
        return arrayList;
    }

    @Override
    public ArrayList<AnnotationInfo> getAttributeList() {
        ArrayList<AnnotationInfo> result = new ArrayList<AnnotationInfo>();
        if (table.getPrimaryKey() != null && table.getPrimaryKey().size() > 1 && table.getPrimaryKey().contains(column.getId())) {
            AnnotationInfo info = new AnnotationInfo();
            info.setAnnotationType(AnnotationType.Marker);
            info.setTypeInfo(new TypeInfo("EmbeddedId"));
            result.add(info);
        } else if (column.isIfPrimaryKey()) {
            AnnotationInfo info = new AnnotationInfo();
            info.setAnnotationType(AnnotationType.Marker);
            info.setTypeInfo(new TypeInfo("Id"));
            result.add(info);
            AnnotationInfo annotationInfo = getNormalColumnAttribute();
            result.add(annotationInfo);
        } else {
            AnnotationInfo annotationInfo = getNormalColumnAttribute();
            result.add(annotationInfo);
        }

        return result;
    }

    private AnnotationInfo getNormalColumnAttribute() {
        AnnotationInfo annotationInfo = new AnnotationInfo();
        annotationInfo.setAnnotationType(AnnotationType.Normal);
        annotationInfo.setTypeInfo(new TypeInfo("Column"));
        HashMap map = new HashMap();

        String columnDefination = getColumnDefinition();
        StringLiteral defination = ast.newStringLiteral();
        defination.setLiteralValue(columnDefination);
        map.put("columnDefinition", defination);
        StringLiteral nameStringLiteral = ast.newStringLiteral();

        //MySQL的数据库关键字需要处理
        if (dbType == DbType.MySQL) {
            nameStringLiteral.setLiteralValue("\"" + column.getCode().toLowerCase() + "\"");
        } else if (dbType == DbType.DM) {
            if (column.getCode().equalsIgnoreCase("context") || column.getCode().equalsIgnoreCase("explain")) {
                nameStringLiteral.setLiteralValue("\"" + column.getCode().toUpperCase() + "\"");
            }
        } else if(dbType == DbType.OpenGauss || dbType ==DbType.PgSQL) {
            nameStringLiteral.setLiteralValue("\"" + column.getCode().toLowerCase() + "\"");
        }
        else {
            nameStringLiteral.setLiteralValue(column.getCode());
        }
        map.put("name", nameStringLiteral);


        map.put("nullable", ast.newBooleanLiteral(column.isNullable()));
        map.put("unique", ast.newBooleanLiteral(column.isUnique()));
        annotationInfo.setParams(map);
        return annotationInfo;
    }

    public String getColumnDefinition() {
        //根据数据库类型拼出字段类型
        String columnDefination = getDataTypeStr();
        //旧版的DBO导出工具导出默认值会出现“''::”，其实是空，需要兼容。例如TBGXYS.dbo包含一个默认值"DefaultValue": "''::character varying"
        if (column.getDefaultValue() != null && column.getDefaultValue().length() > 0 && !column.getDefaultValue().contains("''::")) {
            columnDefination += getDefaultStr();
        }
        return columnDefination;
    }

    private String getDataTypeStr() {
        String type = "";

        //从DBO层面适配Oracle的UTF8字符集，扩1.5倍，来源于ZJ、ZYHY会议决定
        if(FileUtils.isUTF8ExtOpen()){
            if(dbType == DbType.Oracle)
            {
                if(column.getDataType() == DataType.Varchar && column.getLength() <= 4000)
                {
                    if(column.getLength() >= 2667)
                    {
                        column.setLength(4000);
                    }
                    else {
                        column.setLength((int)(Math.ceil(column.getLength() * 1.5)));
                    }
                }
                if(column.getDataType() == DataType.NVarchar && column.getLength() <= 2000)
                {
                    if(column.getLength() >= 1333)
                    {
                        column.setLength(2000);
                    }
                    else {
                        column.setLength((int)(Math.ceil(column.getLength() * 1.5)));
                    }
                }
            }
        }
        if (dbType == DbType.Oracle && (column.getDataType() == DataType.Varchar || column.getDataType() == DataType.Char) && column.getLength() > 4000) {
            column.setDataType(DataType.Clob);
            column.setLength(0);
        }
        if (dbType == DbType.Oracle && (column.getDataType() == DataType.NVarchar || column.getDataType() == DataType.Char) && column.getLength() > 2000) {
            column.setDataType(DataType.NClob);
            column.setLength(0);
        }
        if(dbType == DbType.DB2 && (column.getDataType() == DataType.Decimal) && (column.getPrecision() > 31))
        {
            column.setPrecision(31);
        }
        DataType dataType = column.getDataType();

        if (DataTypeConverter.getInstance().IsExistConverMapping(column.getDataTypeStr())) {
            type = DataTypeConverter.getInstance().GetDataType(column.getDataTypeStr(), dbType.toString());

        } else if (dataType == DataType.LongInt) {
            if (dbType == DbType.Oracle) {
                type = "number(20)";
            } else {
                type = "bigint";
            }


        } else if (dataType == DataType.Decimal) {
            type = "decimal(" + column.getPrecision() + "," + column.getScale() + ")";
        } else if (dataType == DataType.SmallInt) {
            type = "smallint";
        } else {
            type = column.getDataTypeStr();
        }

        if (dataType == DataType.Varchar || dataType == DataType.Char || dataType == DataType.NChar || dataType == DataType.NVarchar) {
            type = type + "(" + column.getLength() + ")";
        }
        return type;
    }

    private String getDefaultStr() {
        String defaultStr = " default ";
        String defaultValue = column.getDefaultValue();
        if (defaultValue.contains("'")) {
            try {
                defaultValue = defaultValue.substring(defaultValue.indexOf("'") + 1, defaultValue.lastIndexOf("'") - 1);
            } catch (Exception ex) {
                throw new RuntimeException("获取默认值出错:表为" + table.getCode() + "列为" + column.getCode(), ex);
            }
        }
        if (DatabaseFuncConverter.getInstance().isExistConverMapping(defaultValue)) {
            defaultStr += DatabaseFuncConverter.getInstance().getFuncName(defaultValue, dbType.toString());
        } else if (column.getDataType() == DataType.Int || column.getDataType() == DataType.LongInt || column.getDataType() == DataType.SmallInt || column.getDataType() == DataType.Float) {
            defaultStr += defaultValue;
        } else if (column.getDataType() == DataType.Boolean) {
            if (defaultValue.equalsIgnoreCase("0") || defaultValue.equalsIgnoreCase("false") || defaultValue.equalsIgnoreCase("f")) {
                if (dbType == DbType.PgSQL || dbType == DbType.Oscar || dbType == DbType.Kingbase || dbType == DbType.OpenGauss) {
                    defaultStr += "false";
                } else {
                    defaultStr += "0";
                }
            } else {
                if (dbType == DbType.PgSQL || dbType == DbType.Oscar || dbType == DbType.Kingbase || dbType == DbType.OpenGauss) {
                    defaultStr += "true";
                } else {
                    defaultStr += "1";
                }
            }
        } else if (column.getDataType() == DataType.TimeStamp && dbType == DbType.Oracle) {
            defaultStr += "TO_TIMESTAMP('" + defaultValue + "', 'YYYY-MM-DD HH24:MI:SS.FF')";
        } else if (column.getDataType() == DataType.DateTime && dbType == DbType.Oracle) {
            defaultStr += "TO_DATE('" + defaultValue + "', 'YYYY-MM-DD HH24:MI:SS')";
        } else {
            defaultStr += "'" + defaultValue + "'";
        }
        return defaultStr;
    }
}
