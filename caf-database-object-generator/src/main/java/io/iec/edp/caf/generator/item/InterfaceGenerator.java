/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.generator.item;

import java.util.ArrayList;

import org.eclipse.jdt.core.dom.Modifier.ModifierKeyword;

import io.iec.edp.caf.generator.baseInfo.CompilationUnitInfo;
import io.iec.edp.caf.generator.baseInfo.InterfaceGenerateInfo;
import io.iec.edp.caf.generator.baseInfo.TypeGeneratorInfo;

/**
 * @author liu_wei
 */
public abstract class InterfaceGenerator extends ItemInfoGenerator {

    protected InterfaceGeneratorContext getContext() {
        return (InterfaceGeneratorContext) super.getContext();
    }

    // region 初始化

    protected InterfaceGenerator(CompilationUnitInfo projectInfo) {
        super(projectInfo);
    }

    // endregion
    @Override
    protected ItemGeneratorContext createItemInfoContext() {
        InterfaceGeneratorContext context = createInterfaceItemContext();
        return context;
    }

    @Override
    protected TypeGeneratorInfo createItemInfo() {
        return InterfaceGenerateInfo.create();
    }

    protected abstract InterfaceGeneratorContext createInterfaceItemContext();

    @Override
    protected void setAccessModifier() {
        getContext().getItemInfo().setModifiers(getAccessModifier());
    }

    protected abstract ArrayList<ModifierKeyword> getAccessModifier();

    // region AfterGenerate
    @Override
    protected void afterGenerate() {
        compileUnit.addType(getContext().getItemInfo().getTypeResult());
    }

    // endregion

}
