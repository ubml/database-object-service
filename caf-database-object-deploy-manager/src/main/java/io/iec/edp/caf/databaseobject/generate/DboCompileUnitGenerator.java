/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.generate;

import io.iec.edp.caf.databaseobject.api.configuration.deplpyconfig.DboDeployConfiguration;
import io.iec.edp.caf.databaseobject.helper.DboDeployConfigHelper;
import io.iec.edp.caf.generator.BaseGenerator;
import io.iec.edp.caf.generator.compileunit.CompileUnitGenerator;
import io.iec.edp.caf.generator.compileunit.CompileUnitGeneratorContext;
import io.iec.edp.caf.databaseobject.api.entity.DbType;
import io.iec.edp.caf.databaseobject.api.entity.AbstractDatabaseObject;
import io.iec.edp.caf.databaseobject.api.entity.DatabaseObjectTable;
import io.iec.edp.caf.databaseobject.api.entity.DatabaseObjectType;

import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * @author liu_wei
 */
public class DboCompileUnitGenerator extends CompileUnitGenerator {
    private AbstractDatabaseObject databaseObject;
    private DboCompileUnitGeneratorContext context;
    private DbType dbType;
    //编译的路径
    private String generatorPath;

    public DbType getDbType() {
        return dbType;
    }

    public void setDbType(DbType dbType) {
        this.dbType = dbType;
    }

    public void setDatabaseObject(AbstractDatabaseObject databaseObject) {
        this.databaseObject = databaseObject;
    }

    private String fileName;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getGeneratorPath() {
        return generatorPath;
    }

    public void setGeneratorPath(String path) {
        this.generatorPath = path;
    }

    @Override
    protected CompileUnitGeneratorContext createProjectInfoContext() {
        context = new DboCompileUnitGeneratorContext();
        context.setAbstractDatabaseObject(databaseObject);
        return context;
    }

    @Override
    protected CompileUnitGeneratorContext getContext() {

        return context;
    }

    @Override
    protected String getPackageName() {

        return "dbo";
    }

    @Override
    protected String getFilePath() {
        String runtimePath = System.getProperty("user.dir");
        DboDeployConfiguration configuration = DboDeployConfigHelper.getInstance().getObjectNameMaxLengthConfiguration();
        return Paths.get(runtimePath).resolve(configuration.getJavaPath()).resolve(generatorPath).toString();
    }

    @Override
    protected ArrayList<String> addImports() {
        ArrayList<String> imports = new ArrayList<String>();
        imports.add("javax.persistence.Table");
        imports.add("javax.persistence.Entity");
        imports.add("javax.persistence.Index");
        imports.add("javax.persistence.Id");
        imports.add("javax.persistence.Column");
        imports.add("javax.persistence.Embeddable");
        imports.add("javax.persistence.EmbeddedId");
        imports.add("javax.persistence.Basic");
        imports.add("java.io.Serializable");
//		imports.add("lombok.Getter");
//		imports.add("lombok.Setter");
        imports.add("java.util.Objects");
        return imports;
    }

    @Override
    protected ArrayList<BaseGenerator> createChildGenerators() {
        ArrayList<BaseGenerator> result = new ArrayList<>();
        DboClassGenerator generator = new DboClassGenerator(getContext().getCompilationUnitInfo());
        generator.setDatabaseObject(databaseObject);
        generator.setDbType(dbType);
        generator.setFileName(fileName);
        result.add(generator);
        if (databaseObject.getType() == DatabaseObjectType.Table || databaseObject.getType() == DatabaseObjectType.TempTable) {
            DatabaseObjectTable table = (DatabaseObjectTable) databaseObject;
            if (table.getPrimaryKey() != null && table.getPrimaryKey().size() > 1) {
                PrimaryKeyClassGenerator pkClassGenerator = new PrimaryKeyClassGenerator(getContext().getCompilationUnitInfo());
                pkClassGenerator.setAbstractDatabaseObject(table);
                pkClassGenerator.setDbType(dbType);
                result.add(pkClassGenerator);
            }
        }

        return result;
    }
}
