/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.generator.baseInfo;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.Modifier.ModifierKeyword;

/**
 * @author liu_wei
 */
public abstract class MethodGenerateInfo extends BaseGenerateInfo {

    private TypeInfo returnType;
    private String methodName;
    private List<ModifierKeyword> modifiers;
    private Boolean isConstructor = false;
    private List<AnnotationInfo> attributes;
    private List<ParameterInfo> params;

    protected MethodDeclaration methodResult;

    public void setAttributes(List<AnnotationInfo> attributes) {
        this.attributes = attributes;
    }

    public void setModifiers(List<ModifierKeyword> modifiers) {
        this.modifiers = modifiers;
    }

    public void setParams(List<ParameterInfo> params) {
        this.params = params;
    }

    public MethodDeclaration getMethodResult() {
        return methodResult;
    }

    public MethodGenerateInfo() {
        modifiers = new ArrayList<ModifierKeyword>();
        params = new ArrayList<ParameterInfo>();
        attributes = new ArrayList<AnnotationInfo>();
    }

    public void setReturnType(TypeInfo returnType) {
        this.returnType = returnType;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public void setIsConstructor(Boolean isConstructor) {
        this.isConstructor = isConstructor;
    }

    void addModifier(ModifierKeyword keyword) {
        if (!modifiers.contains(keyword))
            modifiers.add(keyword);
    }

    void addParameterInfo(ParameterInfo paramInfo) {
        if (!params.contains(paramInfo))
            params.add(paramInfo);
    }

    void addAnnotationInfo(AnnotationInfo paramInfo) {
        if (attributes == null) {
            attributes = new ArrayList<AnnotationInfo>();
        }
        if (!attributes.contains(paramInfo))
            attributes.add(paramInfo);
    }

    @Override
    public void generate() {
        methodResult = ast.newMethodDeclaration();
        methodResult.setConstructor(isConstructor);
        methodResult.setName(ast.newSimpleName(methodName));
        if (returnType != null) {
            methodResult.setReturnType2(returnType.getType(ast));
        }
        if (attributes != null) {
            for (AnnotationInfo attributeInfo : attributes) {
                methodResult.modifiers().add(attributeInfo.getAnnotation(ast));
            }
        }
        if (modifiers != null) {
            for (ModifierKeyword modifier : modifiers) {
                methodResult.modifiers().add(ast.newModifier(modifier));
            }
        }
        if (params != null) {
            for (ParameterInfo paramInfo : params) {
                methodResult.parameters().add(paramInfo.getVariable(ast));
            }
        }
    }

}
