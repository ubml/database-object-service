/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.rpcapi.service;

import io.iec.edp.caf.databaseobject.api.entity.AbstractDatabaseObject;
import io.iec.edp.caf.databaseobject.api.entity.DboDeployResultInfo;
import io.iec.edp.caf.rpc.api.annotation.GspReturnSerializeType;
import io.iec.edp.caf.rpc.api.annotation.GspServiceBundle;
import io.iec.edp.caf.rpc.api.annotation.RpcParam;
import io.iec.edp.caf.rpc.api.common.GspSerializeType;

import java.util.List;
import java.util.Map;

/**
 * @author liu_wei
 */
@GspServiceBundle(applicationName = "runtime", serviceUnitName = "common", serviceName = "databaseobjectrpc")
public interface DatabaseObjectRpcService {

    /**
     * 获取运行时dbo
     *
     * @param dboId id
     * @return 数据库对象
     */
    @GspReturnSerializeType(paramSerializeType = GspSerializeType.Custom, customSerializeTypeRef ="io.iec.edp.caf.databaseobject.rtmanager.rpc.DboSerializer")
    AbstractDatabaseObject getDatabaseObject(@RpcParam(paramName = "dboId") String dboId);

    /**
     * 根据数据库对象编号获取数据库对象
     *
     * @param dboCode 数据库对象编号
     * @return 数据库对象列表
     */
    @GspReturnSerializeType(paramSerializeType = GspSerializeType.Custom, customSerializeTypeRef ="io.iec.edp.caf.databaseobject.rtmanager.rpc.DboSerializer")
    AbstractDatabaseObject getDatabaseObjectByCode(@RpcParam(paramName = "dboCode") String dboCode);

    /**
     * 获取默认DBO实体列表
     *
     * @return DBO实体列表
     */
    //@GspReturnSerializeType(paramSerializeType = GspSerializeType.Custom, customSerializeTypeRef ="io.iec.edp.caf.databaseobject.rtmanager.rpc.DboSerializer")
    //List<AbstractDatabaseObject> getDatabaseObjectList();

    /**
     * 获取设计时DBO
     *
     * @param dboId id
     * @return DBO
     */
    @GspReturnSerializeType(paramSerializeType = GspSerializeType.Custom, customSerializeTypeRef ="io.iec.edp.caf.databaseobject.rtmanager.rpc.DboSerializer")
    AbstractDatabaseObject getDtDatabaseObject(@RpcParam(paramName = "dboId") String dboId);

    /**
     * 获取设计时DBO
     *
     * @param dboCode code
     * @return DBO
     */
    @GspReturnSerializeType(paramSerializeType = GspSerializeType.Custom, customSerializeTypeRef ="io.iec.edp.caf.databaseobject.rtmanager.rpc.DboSerializer")
    AbstractDatabaseObject getDtDatabaseObjectByCode(@RpcParam(paramName = "dboCode") String dboCode);

    /**
     * 保存
     *
     * @param databaseObject DBO
     * @return RPC结果
     */
    String saveDtDatabaseObject(@RpcParam(paramName = "databaseObject") String databaseObject);

    /**
     * 删除
     *
     * @param dboId id
     * @return RPC结果
     */
    String deleteDtDatabaseObject(@RpcParam(paramName = "dboId") String dboId);

    /**
     * 根据Id判断设计时DBO是否存在
     *
     * @param dboId id
     * @return 结果
     */
    boolean isExistDatabaseObjectDt(@RpcParam(paramName = "dboId") String dboId);

    /**
     * 部署单个DBO
     *
     * @param databaseObject DBO实体
     * @param dimensionInfo  维度信息
     * @return RPC结果
     */
    String deployDatabaseObject(@RpcParam(paramName = "databaseObject") String databaseObject, @RpcParam(paramName = "dimensionInfo") Map<String, String> dimensionInfo);

    /**
     * 部署多个DBO
     *
     * @param databaseObjects DBO实体列表
     * @param dimensionValue 维度信息
     * @return 部署结果
     */
    DboDeployResultInfo deployDatabaseObjects(@RpcParam(paramName = "databaseObjects") String databaseObjects, @RpcParam(paramName = "dimensionValue") Map<String, String> dimensionValue);
}
