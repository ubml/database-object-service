/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.common.database;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author liu_wei
 */
public interface GspDatabase {

    /**
     * 执行单句SQL
     *
     * @param sqlStatement SQL语句
     * @return 执行结果影响的行数
     * @throws SQLException 异常
     */
    int execSqlStatement(String sqlStatement) throws SQLException;

    /**
     * 执行一组SQL
     *
     * @param sqlStatements 一组SQL语句
     * @throws SQLException 异常
     */
    void execSqlStatement(String[] sqlStatements) throws SQLException;


    /**
     * 执行获取数据集的SQL
     *
     * @param sqlStatement SQL语句
     * @return 返回结果集
     * @throws SQLException 异常
     */
    ResultSet executeResultSet(String sqlStatement) throws SQLException;

    /**
     * 根据输入的参数值列表，执行无返回数据集的SQL
     *
     * @param sqlStatement  SQL语句
     * @param paramDataList 参数值列表
     * @throws SQLException 异常
     */
    void execSqlStatement(String sqlStatement, Object[][] paramDataList) throws SQLException;

    /**
     * 关闭数据库链接
     */
    void close();

    /**
     * 开启数据库链接
     */
    void open();

    /**
     * 执行单句SQL，需要手工开启和关闭连接
     *
     * @param sqlStatement SQL语句
     * @throws SQLException 异常
     */
    void execSqlStatementNotAutomatic(String sqlStatement) throws SQLException;
}
