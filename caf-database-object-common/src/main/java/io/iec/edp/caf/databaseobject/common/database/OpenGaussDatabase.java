package io.iec.edp.caf.databaseobject.common.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class OpenGaussDatabase extends AbstractDatabaseImpl {

    public OpenGaussDatabase(DbConfigData configData) {
        super(configData);
    }

    @Override
    public Connection getConnection(DbConfigData configData) throws SQLException {
        Connection c = null;
        String url = configData.getConnectionString();

        try {
            Class.forName("org.opengauss.Driver");
            c = DriverManager.getConnection(url, configData.getUserId(), configData.getPassword());
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("构造GUSSDB数据库连接出错，找不到Driver:" + e);
        }

        return c;
    }
}