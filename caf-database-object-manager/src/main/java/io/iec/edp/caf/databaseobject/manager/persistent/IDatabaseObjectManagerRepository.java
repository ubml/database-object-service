/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.manager.persistent;

import io.iec.edp.caf.databaseobject.api.entity.AbstractDatabaseObject;

import java.util.ArrayList;

/**
 * @author liu_wei
 */
public interface IDatabaseObjectManagerRepository {
    /**
     * 新建DBO文件
     *
     * @param fullPath 全路径
     * @param object   实体
     */
    void createDatabaseObject(String fullPath, AbstractDatabaseObject object);

    /**
     * 更新DBO文件
     *
     * @param fullPath 全路径
     * @param object   实体
     */
    void updateDatabaseObject(String fullPath, AbstractDatabaseObject object);

    /**
     * 文件是否存在
     *
     * @param fullPath 全路径
     * @return 是否
     */
    boolean isFileExist(String fullPath);

    /**
     * 读取文件内容
     *
     * @param fullPath 全路径
     * @return 文件内容
     */
    String read(String fullPath);

    /**
     * 创建文件夹
     *
     * @param path 文件夹路径
     */
    void createFolder(String path);

    /**
     * 获取指定文件夹下的所有文件全路径
     *
     * @param path 文件夹路径
     * @return 所有文件全路径
     */
    ArrayList<String> getFilePaths(String path);

    /**
     * 文件夹是否存在
     *
     * @param path 文件夹路径
     * @return 是否
     */
    boolean isDirectoryExist(String path);
}
