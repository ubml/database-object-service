/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.databaseobject.sqlservergenerator;

import io.iec.edp.caf.databaseobject.defaultsqlgenerator.AbstractDefaultSqlGenerator;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liu_wei
 */
public class SqlServerSqlReflect extends AbstractDefaultSqlGenerator {
    @Override
    public List<String> GetViewSql(String viewName, String defination) {
        List<String> result = new ArrayList<String>();
        String dropSql = String.format("IF  EXISTS(SELECT 1 FROM SYSOBJECTS WHERE ID = OBJECT_ID('%1$s') AND TYPE ='V')" + "\r\n" +
                "DROP VIEW %2$s " + "\r\n" +
                "", viewName, viewName);
        result.add(dropSql);
        String createSql = String.format("Create View %1$s AS ", viewName) + defination;
        result.add(createSql);
        return result;
    }

    @Override
    public String getReNameColumnSql(String tableName, String oldColName, String newColName) {
        //exec sp_rename '[表名].[列名]','[新列名]'
        return "exec sp_rename " + "'" + tableName + "." + oldColName + "'" + "," + "'" + newColName + "'";
    }

    @Override
    public String getDefaultValue(String schemaName, String tableName, String colName) {
        return "SELECT SM.TEXT AS defaultvalue FROM dbo.sysobjects SO INNER JOIN dbo.syscolumns SC ON SO.id = SC.id LEFT JOIN dbo.syscomments SM ON SC.cdefault = SM.id WHERE SO.xtype = 'U' and so.name= '" + tableName + "' and SC.NAME = '" + colName + "'";
    }
}
