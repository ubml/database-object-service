/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.generator.module;

import java.io.IOException;
import java.util.ArrayList;

import io.iec.edp.caf.generator.BaseGenerator;
import io.iec.edp.caf.generator.BaseGeneratorContext;
import io.iec.edp.caf.generator.compileunit.CompileUnitGenerator;
import io.iec.edp.caf.generator.utils.CompilerUtils;
import io.iec.edp.caf.generator.utils.JarUtils;

/**
 * @author liu_wei
 */
public abstract class ModuleGenerator extends BaseGenerator {

    private ArrayList<BaseGenerator> childGens = new ArrayList<BaseGenerator>();

    protected ModuleGeneratorContext getContext() {
        return (ModuleGeneratorContext) super.getContext();
    }

    @Override
    protected BaseGeneratorContext createContext() {
        ModuleGeneratorContext context = createModuleGeneratorContext();
        return context;
    }

    protected ModuleGeneratorContext createModuleGeneratorContext() {
        return new ModuleGeneratorContext();
    }

    @Override
    protected void doGenerate() {
        getContext().getModuleInfo().generate();
    }

    @Override
    protected final ArrayList<BaseGenerator> createChildGenerators() {
        ArrayList<BaseGenerator> childGenerators = super.createChildGenerators();
        if (childGenerators == null) {
            childGenerators = new ArrayList<BaseGenerator>();
        }

        if (childGens.size() > 0)
            childGenerators.addAll(childGens);
        addChildGenrators(childGenerators);

        return childGenerators;
    }

    private void addChildGenrators(ArrayList<BaseGenerator> generators) {
        ArrayList<CompileUnitGenerator> childUnitGenerators = getCompileUnitGenrators();
        if (childUnitGenerators != null && childUnitGenerators.size() > 0) {
            generators.addAll(childUnitGenerators);
        }

        ArrayList<BaseGenerator> extendGenerator = getExtendGenrators();
        if (extendGenerator != null && extendGenerator.size() > 0) {
            generators.addAll(extendGenerator);
        }
    }

    protected ArrayList<CompileUnitGenerator> getCompileUnitGenrators() {
        return null;
    }

    protected ArrayList<BaseGenerator> getExtendGenrators() {
        return null;
    }

    public void addCompileUnitGenrator(CompileUnitGenerator child) {
        childGens.add(child);
    }

    public void addExtendChildGenrator(BaseGenerator child) {
        childGens.add(child);
    }

    public void compile(String sourcePath, String targetPath, ArrayList<String> jarPathes) {
        try {
            CompilerUtils.compiler(sourcePath, targetPath, sourcePath, "UTF-8", jarPathes);
        } catch (Exception e) {
            throw new RuntimeException("编译失败" + e.toString(), e);
        }
    }


    public void build(String sourcePath, String targetPath, ArrayList<String> jarPathes, String jarFileName, String jarFilePath) {
        compile(sourcePath, targetPath, jarPathes);
        try {
            JarUtils.createTempJar(targetPath, jarFilePath, jarFileName);
        } catch (IOException e) {
            throw new RuntimeException("打包失败");
        }
    }
}
