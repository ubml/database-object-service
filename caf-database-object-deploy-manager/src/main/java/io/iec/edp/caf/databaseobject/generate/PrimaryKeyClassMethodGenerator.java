/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.generate;

import io.iec.edp.caf.databaseobject.DataTypeUtil;
import io.iec.edp.caf.generator.baseInfo.ParameterInfo;
import io.iec.edp.caf.generator.baseInfo.TypeInfo;
import io.iec.edp.caf.generator.method.ClassMethodGenerator;
import io.iec.edp.caf.databaseobject.api.entity.DatabaseObjectColumn;
import io.iec.edp.caf.databaseobject.api.entity.DatabaseObjectTable;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.FieldAccess;
import org.eclipse.jdt.core.dom.Modifier;

import java.util.ArrayList;

/**
 * @author liu_wei
 */
public class PrimaryKeyClassMethodGenerator extends ClassMethodGenerator {
    private DatabaseObjectTable table;

    protected PrimaryKeyClassMethodGenerator(DatabaseObjectTable table) {
        this.table = table;
    }

    @Override
    protected boolean buildIsConstructor() {
        return true;
    }

    @Override
    protected Block buildMethodBody() {
        Block block = ast.newBlock();
        for (String primaryKey : table.getPrimaryKey()) {
            DatabaseObjectColumn column = table.getColumnById(primaryKey);
            Assignment assignment = ast.newAssignment();
            FieldAccess fieldAccess = ast.newFieldAccess();
            fieldAccess.setExpression(ast.newThisExpression());
            fieldAccess.setName(ast.newSimpleName(column.getCode()));
            assignment.setLeftHandSide(fieldAccess);
            assignment.setRightHandSide(ast.newSimpleName(column.getCode()));
            assignment.setOperator(Assignment.Operator.ASSIGN);
            block.statements().add(ast.newExpressionStatement(assignment));
        }
        return block;
    }

    @Override
    protected String getMethodName() {
        return table.getCode() + "Id";
    }

    @Override
    protected TypeInfo getReturnType() {
        return null;
    }

    @Override
    protected ArrayList<Modifier.ModifierKeyword> getAccessModifiers() {
        return null;
    }

    @Override
    protected ArrayList<ParameterInfo> getParameterCollection() {
        ArrayList<ParameterInfo> parameterInfos = new ArrayList<>();
        for (String primaryKey : table.getPrimaryKey()) {
            DatabaseObjectColumn column = table.getColumnById(primaryKey);
            ParameterInfo parameterInfo = new ParameterInfo();
            parameterInfo.setParamName(column.getCode());
            DataTypeUtil util = new DataTypeUtil();
            parameterInfo.setParamType(util.getTypeInfoByDataType(column.getDataType()));
            parameterInfos.add(parameterInfo);
        }
        return parameterInfos;
    }
}
