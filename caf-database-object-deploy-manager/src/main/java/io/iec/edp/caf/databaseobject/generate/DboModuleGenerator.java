/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.generate;

import io.iec.edp.caf.generator.BaseGeneratorContext;
import io.iec.edp.caf.generator.compileunit.CompileUnitGenerator;
import io.iec.edp.caf.generator.module.ModuleGenerator;
import io.iec.edp.caf.generator.module.ModuleGeneratorContext;
import io.iec.edp.caf.databaseobject.api.entity.DbType;
import io.iec.edp.caf.databaseobject.api.entity.AbstractDatabaseObject;

import java.util.ArrayList;

/**
 * @author liu_wei
 */
public class DboModuleGenerator extends ModuleGenerator {
    public DboModuleGenerator() {
    }

    private AbstractDatabaseObject abstractDatabaseObject;
    private DboModuleGeneratorContext context;
    private DbType dbType;
    //编译的路径
    private String generatorPath;

    public void setAbstractDatabaseObject(AbstractDatabaseObject abstractDatabaseObject) {
        this.abstractDatabaseObject = abstractDatabaseObject;
    }

    public DbType getDbType()
    {
        return dbType;
    }

    public void setDbType(DbType dbType) {
        this.dbType = dbType;
    }

    private String fileName;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getGeneratorPath() {
        return generatorPath;
    }

    public void setGeneratorPath(String path) {
        this.generatorPath = path;
    }

    @Override
    protected BaseGeneratorContext createContext() {
        context = new DboModuleGeneratorContext();
        context.setAbstractDatabaseObject(this.abstractDatabaseObject);
        return context;
    }

    @Override
    protected ModuleGeneratorContext getContext() {
        context = new DboModuleGeneratorContext();
        context.setAbstractDatabaseObject(this.abstractDatabaseObject);
        return context;
    }

    @Override
    protected ArrayList<CompileUnitGenerator> getCompileUnitGenrators() {
        ArrayList<CompileUnitGenerator> compileUnitGenerators = new ArrayList<CompileUnitGenerator>();
        DboCompileUnitGenerator generator = new DboCompileUnitGenerator();
        generator.setDatabaseObject(abstractDatabaseObject);
        generator.setDbType(dbType);
        generator.setFileName(fileName);
        generator.setGeneratorPath(generatorPath);
        compileUnitGenerators.add(generator);
        return compileUnitGenerators;
    }
}
