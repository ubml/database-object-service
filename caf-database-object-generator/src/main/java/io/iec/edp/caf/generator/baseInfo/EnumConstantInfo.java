/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.generator.baseInfo;

import java.util.List;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.EnumConstantDeclaration;
import org.eclipse.jdt.core.dom.Expression;

/**
 * @author liu_wei
 */
public class EnumConstantInfo extends BaseGenerateInfo {

    private String enumName;
    private List<Expression> arguments;

    public void setEnumName(String enumName) {
        this.enumName = enumName;
    }

    public void setArguments(List<Expression> arguments) {
        this.arguments = arguments;
    }

    private EnumConstantDeclaration enumConstrantResult;

    @Override
    void generate() {
        enumConstrantResult = ast.newEnumConstantDeclaration();
        enumConstrantResult.setName(ast.newSimpleName(enumName));
        if (arguments != null)
            enumConstrantResult.arguments().addAll(arguments);
    }

    public EnumConstantDeclaration getEnumConstant(AST ast) {
        this.ast = ast;
        generate();
        return enumConstrantResult;
    }
}
