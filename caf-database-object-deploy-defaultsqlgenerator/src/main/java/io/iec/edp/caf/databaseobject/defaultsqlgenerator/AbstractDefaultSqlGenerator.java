/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.iec.edp.caf.databaseobject.defaultsqlgenerator;

import io.iec.edp.caf.databaseobject.IDatabaseObjectSQLReflect;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author liu_wei
 */
public abstract class AbstractDefaultSqlGenerator implements IDatabaseObjectSQLReflect {
    @Override
    public List<String> GetViewSql(String viewName, String defination) {
        List<String> result = new ArrayList<String>();
        String sql = "Create or Replace View " + viewName + " AS " + defination;
        result.add(sql);
        return result;
    }

    @Override
    public String getReNameColumnSql(String tableName, String oldColName, String newColName) {
        return "ALTER TABLE " + tableName + " RENAME COLUMN " + oldColName + " TO " + newColName;
    }

    @Override
    public String getDropColumnSql(String tableName, String colName) {
        return "ALTER TABLE " + tableName + " DROP COLUMN " + colName;
    }

    @Override
    public String getDefaultValue(String schemaName, String tableName, String colName) {
        return "select utc.data_default as defaultvalue from user_tab_columns utc, user_col_comments ucc where utc.table_name = ucc.table_name and utc.column_name = ucc.column_name and utc.table_name = upper('" + tableName + "') and utc.column_name = upper('" + colName + "')";
    }
}
