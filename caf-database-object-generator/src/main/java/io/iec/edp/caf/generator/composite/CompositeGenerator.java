/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.generator.composite;

import java.util.ArrayList;

import io.iec.edp.caf.generator.BaseGenerator;
import io.iec.edp.caf.generator.BaseGeneratorContext;

/**
 * @author liu_wei
 */
public abstract class CompositeGenerator extends BaseGenerator {

    protected CompositeGeneratorContext getContext() {
        return (CompositeGeneratorContext) super.getContext();
    }

    private ArrayList<BaseGenerator> compositeItemGenerators = new ArrayList<BaseGenerator>();

    @Override
    protected final BaseGeneratorContext createContext() {
        return createCompositeContext();
    }

    protected abstract CompositeGeneratorContext createCompositeContext();

//	@Override
//	protected final void doInitialize() {
//		compositeItemGenerators = createCompositeItemGenerators();
//
//		for (BaseGenerator itemGenerator : compositeItemGenerators) {
//			itemGenerator.initialize();
//		}
//	}

    @Override
    protected ArrayList<BaseGenerator> createChildGenerators() {
        return createCompositeItemGenerators();
    }

    protected abstract ArrayList<BaseGenerator> createCompositeItemGenerators();

//	@Override
//	protected void doGenerate() {
//
//		for (BaseGenerator itemGenerator : compositeItemGenerators) {
//			itemGenerator.generate();
//		}
//	}
}
