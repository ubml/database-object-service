/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.generator.enumGen;

import io.iec.edp.caf.generator.baseInfo.EnumGenerateInfo;
import io.iec.edp.caf.generator.baseInfo.TypeGeneratorInfo;
import io.iec.edp.caf.generator.item.ItemGeneratorContext;

/**
 * @author liu_wei
 */
public class EnumGeneratorContext extends ItemGeneratorContext {
    private EnumGenerateInfo enumInfo;

    protected EnumGenerateInfo getEnumInfo() {
        return enumInfo;
    }

    @Override
    protected void setItemInfo(TypeGeneratorInfo value) {
        enumInfo = (EnumGenerateInfo) value;
        super.setItemInfo(value);
    }

    @Override
    protected TypeGeneratorInfo getItemInfo() {
        return getEnumInfo();
    }

}
