/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.generator.property;

import java.util.ArrayList;

import io.iec.edp.caf.generator.BaseGenerator;
import io.iec.edp.caf.generator.BaseGeneratorContext;
import io.iec.edp.caf.generator.baseInfo.AnnotationInfo;
import io.iec.edp.caf.generator.baseInfo.PropertyGenerateInfo;
import io.iec.edp.caf.generator.baseInfo.TypeInfo;

/**
 * @author liu_wei
 */
public abstract class PropertyGenerator extends BaseGenerator {

    protected PropertyGeneratorContext getContext() {
        return (PropertyGeneratorContext) super.getContext();
    }

    @Override
    protected final BaseGeneratorContext createContext() {
        PropertyGeneratorContext context = createPropertyGeneratorContext();
        PropertyGenerateInfo info = createPropertyInfo();
        info.setAst(ast);
        context.setPropertyInfo(info);
        return context;
    }

    protected PropertyGeneratorContext createPropertyGeneratorContext() {
        return new PropertyGeneratorContext();
    }

    protected abstract PropertyGenerateInfo createPropertyInfo();

    @Override
    protected void beforeGenerate() {
        TypeInfo propertyType = getPropertyType();
        getContext().getPropertyInfo().setPropertyType(propertyType);
        getContext().getPropertyInfo().setFieldName(getFieldName());
        getContext().getPropertyInfo().setPropertyName(getPropertyName());

        ArrayList<AnnotationInfo> getAttrs = getGetterAttributeList();
        ArrayList<AnnotationInfo> setAttrs = getSetterAttributeList();
        getContext().getPropertyInfo().setGettrtAttributes(getAttrs);
        getContext().getPropertyInfo().setSettrtAttributes(setAttrs);

        getContext().getPropertyInfo().setHasGetMethod(hasGetMethod());
        getContext().getPropertyInfo().setHasSetMethod(hasSetMethod());


        if (propertyType != null) {
            String typeFullName = propertyType.getTypeFullName();
            if (typeFullName != null)
                addImport(typeFullName);
        }

        if (getAttrs != null) {
            for (AnnotationInfo attr :
                    getAttrs) {
                String typeFullName = attr.getTypeInfo().getTypeFullName();
                if (typeFullName != null)
                    addImport(typeFullName);
            }
        }

        if (setAttrs != null) {
            for (AnnotationInfo attr :
                    setAttrs) {
                String typeFullName = attr.getTypeInfo().getTypeFullName();
                if (typeFullName != null)
                    addImport(typeFullName);
            }
        }
    }

    @Override
    protected void doGenerate() {
        getContext().getPropertyInfo().generate();
    }

    protected abstract TypeInfo getPropertyType();

    protected String getFieldName() {
        return null;
    }

    protected ArrayList<AnnotationInfo> getGetterAttributeList() {
        return null;
    }

    protected ArrayList<AnnotationInfo> getSetterAttributeList() {
        return null;
    }

    protected abstract String getPropertyName();

    protected boolean hasGetMethod() {
        return true;
    }

    protected boolean hasSetMethod() {
        return true;
    }

}
