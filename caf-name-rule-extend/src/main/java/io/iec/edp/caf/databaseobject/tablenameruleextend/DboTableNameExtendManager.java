/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.tablenameruleextend;


import io.iec.edp.caf.boot.context.CAFContext;
import io.iec.edp.caf.databaseobject.api.entity.DBOTableNameRule;
import io.iec.edp.caf.databaseobject.api.entity.DimensionStrategyWithOrder;
import io.iec.edp.caf.databaseobject.common.DatabaseObjectCommonUtil;
import io.iec.edp.caf.databaseobject.spi.ITableNameRuleManager;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author zhaoleitr
 */
public class DboTableNameExtendManager implements ITableNameRuleManager {
    @Override
    public final List<String> getTableNames(String dboCode, DBOTableNameRule dboTableNameRule, Map<String, List<String>> dimensionInfo) {
        checkForEmptyString(dboCode, "dimensioinInfo");
        checkForNullReference(dboTableNameRule, "dboTableNameRule");
        if (dimensionInfo == null || dimensionInfo.size() <= 0) {
            dimensionInfo = getDefaultDimensionInfo();
            if (dimensionInfo.size() <= 0) {
                throw new RuntimeException("维度值信息不能为空。");
            }
            //如果只有年度维度，且年度值还是空，抛异常
            if (dimensionInfo.size() == 1 && dimensionInfo.get("FIYear").size() == 0) {
                throw new RuntimeException("获取默认年度集合为空，fiyear表中不存在年度数据。");
            }
        }
        if (dboTableNameRule.getDimensionStrategyWithOrder() == null || dboTableNameRule.getDimensionStrategyWithOrder().size() <= 0) {
            throw new RuntimeException(String.format("表名规则中维度策略不能为空，dbo编号为：%1$s，表名规则标识为%2$s", dboCode, dboTableNameRule.getId()));
        }

        List<String> tableNames = new ArrayList<>();
        List<String> currentTableNames = new ArrayList<>();
        sortStategy(dboTableNameRule.getDimensionStrategyWithOrder());
        currentTableNames.add(dboCode);
        //存在多个维度，每个维度可能存在多个值的场景
        for (DimensionStrategyWithOrder strategy : dboTableNameRule.getDimensionStrategyWithOrder()) {
            if (!dimensionInfo.containsKey(strategy.getDimensionEntity().getCode())) {
                throw new RuntimeException(String.format("维度值信息中不包含所需的维度信息，维度key值为%1$s", strategy.getDimensionEntity().getCode()));
            }
            currentTableNames = getTableNames(dimensionInfo.get(strategy.getDimensionEntity().getCode()), currentTableNames);
        }
        if (currentTableNames != null && !currentTableNames.isEmpty()) {
            tableNames.addAll(currentTableNames);
        }
        tableNames.remove(dboCode);
        return tableNames;
    }

    private Map<String, List<String>> getDefaultDimensionInfo() {
        DatabaseObjectCommonUtil commonUtil = new DatabaseObjectCommonUtil();
        return commonUtil.getDefaultDimensionInfo(commonUtil.getCurrentDbInfo());
    }

    /**
     * 维度策略排序（针对表名规则中存在多个维度的场景）
     *
     * @param strategies 策略
     */
    private void sortStategy(List<DimensionStrategyWithOrder> strategies) {
        for (int i = 1; i < strategies.size() - 1; i++) {
            for (int j = 0; j < strategies.size() - i - 1; j++) {
                if (strategies.get(j).getOrder() > strategies.get(j + 1).getOrder()) {
                    DimensionStrategyWithOrder c = strategies.get(j + 1);
                    strategies.set(j + 1, strategies.get(j));
                    strategies.set(j, c);
                }
            }
        }
    }

    private static List<String> getTableNames(List<String> dimensionValues, List<String> tableNames) {
        List<String> result = new ArrayList<>();

        if (dimensionValues != null && !dimensionValues.isEmpty()) {
            for (String value : dimensionValues) {
                for (String tableName : tableNames) {
                    result.add(tableName + value);
                }
            }
        }
        return result;
    }

    @Override
    public final String getTableName(String dboCode, DBOTableNameRule dboTableNameRule, Map<String, String> dimensionValue) {
        checkForEmptyString(dboCode, "dboCode");
        checkForNullReference(dboTableNameRule, "dboTableNameRule");

        if (dimensionValue == null || dimensionValue.size() <= 0) {
            if (("FIYear").equalsIgnoreCase(dboTableNameRule.getCode())) {
                return getTableNameWithSession(dboCode, dboTableNameRule);
            } else if (("YYMM").equalsIgnoreCase(dboTableNameRule.getCode())) {
                Date date = new Date();
                return dboCode + new SimpleDateFormat("yyMM").format(date);
            }
        }
        if(dimensionValue != null && !dimensionValue.containsKey("YYMM")){
            if(("YYMM").equalsIgnoreCase(dboTableNameRule.getCode())){
                Date date =new Date();
                return dboCode + new SimpleDateFormat("yyMM").format(date);
            }
        }
        if (dboTableNameRule.getDimensionStrategyWithOrder() == null || dboTableNameRule.getDimensionStrategyWithOrder().size() <= 0) {
            throw new RuntimeException("表名规则中维度策略不能为空，dbo编号为：" + dboCode + "，表名规则标识为" + dboTableNameRule.getId());
        }

        String tableName = dboCode;
        sortStategy(dboTableNameRule.getDimensionStrategyWithOrder());
        for (DimensionStrategyWithOrder strategy : dboTableNameRule.getDimensionStrategyWithOrder()) {
            if (!dimensionValue.containsKey(strategy.getDimensionEntity().getCode())) {
                throw new RuntimeException(String.format("维度值信息中不包含所需的维度信息，维度key值为%1$s", strategy.getDimensionEntity().getCode()));
            }
            tableName += dimensionValue.get(strategy.getDimensionEntity().getCode());
        }
        return tableName;
    }

    private String getTableNameWithSession(String tableName, DBOTableNameRule dboTableNameRule) {
        sortStategy(dboTableNameRule.getDimensionStrategyWithOrder());
        for (DimensionStrategyWithOrder strategy : dboTableNameRule.getDimensionStrategyWithOrder()) {
            try {
                String code = strategy.getDimensionEntity().getCode();
                String value = CAFContext.current.getBizContext().get(code).toString();
                if (value == null || value.length() <= 0) {
                    value = String.valueOf(java.time.LocalDateTime.now().getYear());
                }
                tableName = tableName + value;
            } catch (Exception e) {
                //吞掉异常；if判断没起作用，暂不需要。
                //if (strategy.DimensionEntity.Code == "FIYear")
                tableName += java.time.LocalDateTime.now().getYear();
            }

        }

        return tableName;
    }

    private void checkForEmptyString(String strValue, String strName) throws RuntimeException {
        if (strValue == null || strValue.length() <= 0) {
            throw new RuntimeException(strName + "不能为空");
        }
    }

    private void checkForNullReference(Object objectValue, String objectName) throws RuntimeException {
        if (objectValue == null) {
            throw new RuntimeException(objectName + "不能为null");
        }
    }
}
