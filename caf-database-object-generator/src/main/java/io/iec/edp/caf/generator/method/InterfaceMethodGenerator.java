/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.generator.method;

import io.iec.edp.caf.generator.baseInfo.InterfaceMethodGenerateInfo;
import io.iec.edp.caf.generator.baseInfo.MethodGenerateInfo;
import io.iec.edp.caf.generator.item.InterfaceGenerator;

/**
 * @author liu_wei
 */
public abstract class InterfaceMethodGenerator extends MethodGenerator {

    //CreateMethodInfoContext
    @Override
    protected MethodGeneratorContext createMethodInfoContext() {
        InterfaceMethodGeneratorContext context = createClassInfoContext();
        return context;
    }

    protected InterfaceMethodGeneratorContext createClassInfoContext() {
        return new InterfaceMethodGeneratorContext();
    }

    @Override
    protected MethodGenerateInfo createMethodInfo() {
        return createClassMethodInfo();
    }

    protected InterfaceMethodGenerateInfo createClassMethodInfo() {
        return new InterfaceMethodGenerateInfo();
    }

    // endregion

    @Override
    protected final void afterGenerate() {
        if (parentGenerator != null) {
            ((InterfaceGenerator) parentGenerator).addChildSyntax(getContext().getMethodGenInfo().getMethodResult());
        }
    }

}
