/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject;

import io.iec.edp.caf.databaseobject.api.configuration.datatype.DBODataTypeConfigurationLoader;
import io.iec.edp.caf.databaseobject.api.configuration.datatype.DataTypeMapping;
import io.iec.edp.caf.databaseobject.api.configuration.datatype.DatabaseObjectDataTypeConfiguration;

import java.io.IOException;

/**
 * @author liu_wei
 */
public class DataTypeConverter extends DBODataTypeConfigurationLoader {
    private static DataTypeConverter instance;

    public DataTypeConverter() throws IOException {
    }

    /**
     * 元数据序列化器帮助
     */

    public static DataTypeConverter getInstance() {
        try {
            instance = (instance != null) ? instance : (instance = new DataTypeConverter());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return instance;
    }

    public final boolean IsExistConverMapping(String dataType) {
        DatabaseObjectDataTypeConfiguration data = null;
        try {
            data = GetDatabaseObjectConfiguration(dataType);
        } catch (IOException e) {
            throw new RuntimeException("获取数据库对象数据类型映射配置报错", e);
        }
        if (data != null) {
            return true;
        } else {
            return false;
        }
    }

    public final String GetDataType(String dataType, String dbType) {
        DatabaseObjectDataTypeConfiguration data = null;
        try {
            data = GetDatabaseObjectConfiguration(dataType);
        } catch (IOException e) {
            throw new RuntimeException("获取数据库对象数据类型映射配置报错", e);
        }
        if (data != null) {
            if (data.getDataTypeMappings() != null && data.getDataTypeMappings().size() > 0) {
                DataTypeMapping typeMapping = data.getDataTypeMappings().stream().filter((item) -> item.getDBType().equals(dbType)).findFirst().orElse(null);
                if (typeMapping.getDataType() != null && typeMapping.getDataType().length() > 0) {
                    return typeMapping.getDataType();
                }
            }
        }
        return dataType;
    }
}
