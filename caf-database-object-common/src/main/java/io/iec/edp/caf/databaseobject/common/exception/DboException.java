package io.iec.edp.caf.databaseobject.common.exception;

import io.iec.edp.caf.commons.exception.CAFRuntimeException;
import io.iec.edp.caf.commons.exception.ExceptionLevel;

/**
 * @author xuefengping
 * @date 2023/5/22 18:21
 */
public class DboException extends CAFRuntimeException {
    public DboException(String serviceUnitCode, String resourceFile, String exceptionCode, String[] messageParams, Exception innerException) {
        super(serviceUnitCode, resourceFile, exceptionCode, messageParams, innerException);
    }

    public DboException(String serviceUnitCode, String resourceFile, String exceptionCode, String[] messageParams, Exception innerException, ExceptionLevel level) {
        super(serviceUnitCode, resourceFile, exceptionCode, messageParams, innerException, level);
    }

    public DboException(String serviceUnitCode, String resourceFile, String exceptionCode, String[] messageParams, Exception innerException, ExceptionLevel level, boolean bizException) {
        super(serviceUnitCode, resourceFile, exceptionCode, messageParams, innerException, level, bizException);
    }

    public DboException(String serviceUnitCode, String exceptionCode, String message, Exception innerException) {
        super(serviceUnitCode, exceptionCode, message, innerException);
    }

    public DboException(String serviceUnitCode, String exceptionCode, String message, Exception innerException, ExceptionLevel level) {
        super(serviceUnitCode, exceptionCode, message, innerException, level);
    }

    public DboException(String serviceUnitCode, String exceptionCode, String message, Exception innerException, ExceptionLevel level, boolean bizException) {
        super(serviceUnitCode, exceptionCode, message, innerException, level, bizException);
    }

    public DboException(String exceptionCode, String message, Exception innerException, ExceptionLevel level, boolean bizException) {
        super("Sys", exceptionCode, message, innerException, level, bizException);
    }
}
