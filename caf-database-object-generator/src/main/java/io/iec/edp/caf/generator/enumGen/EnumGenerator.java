/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.generator.enumGen;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.Modifier.ModifierKeyword;

import io.iec.edp.caf.generator.baseInfo.CompilationUnitInfo;
import io.iec.edp.caf.generator.baseInfo.EnumConstantInfo;
import io.iec.edp.caf.generator.baseInfo.EnumGenerateInfo;
import io.iec.edp.caf.generator.baseInfo.TypeGeneratorInfo;
import io.iec.edp.caf.generator.item.ItemGeneratorContext;
import io.iec.edp.caf.generator.item.ItemInfoGenerator;

/**
 * @author liu_wei
 */
public abstract class EnumGenerator extends ItemInfoGenerator {

    protected EnumGeneratorContext getContext() {
        return (EnumGeneratorContext) super.getContext();
    }

    protected EnumGenerator(CompilationUnitInfo projectInfo) {
        super(projectInfo);
    }

    // region BeforeGenerate

    @Override
    protected void beforeGenerate() {
        getContext().getEnumInfo().setEnumConstants(buildEnumCollection());
        super.beforeGenerate();
    }

    protected List<EnumConstantInfo> buildEnumCollection() {
        return null;
    }
    // endregion

    // region CreateItemInfoContext
    @Override
    protected ItemGeneratorContext createItemInfoContext() {
        EnumGeneratorContext context = createEnumInfoContext();
        // ClassGenerateInfo classInfo = ClassGenerateInfo.Create();
        // context.ItemInfo = classInfo;
        return context;
    }

    @Override
    protected TypeGeneratorInfo createItemInfo() {
        return new EnumGenerateInfo();
    }

    protected EnumGeneratorContext createEnumInfoContext() {
        return new EnumGeneratorContext();
    }
    // C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    /// #endregion

    // C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    /// #region AfterGenerate
    @Override
    protected void afterGenerate() {
        compileUnit.addType(getContext().getItemInfo().getTypeResult());
    }

    // C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    /// #endregion

    // C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    /// #region AccessModifier

    @Override
    protected final void setAccessModifier() {
        getContext().getItemInfo().setModifiers(getAccessModifier());
    }

    protected abstract ArrayList<ModifierKeyword> getAccessModifier();
    // endregion

}
