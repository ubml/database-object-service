package io.iec.edp.caf.databaseobject.common.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class GBase8sDatabase extends AbstractDatabaseImpl {

    public GBase8sDatabase(DbConfigData configData) {
        super(configData);
    }

    @Override
    public Connection getConnection(DbConfigData configData) throws SQLException {
        Connection c = null;
        String url = configData.getConnectionString();

        try {
            Class.forName("cn.gbase8s.Driver");
            c = DriverManager.getConnection(url, configData.getUserId(), configData.getPassword());
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("构造GBASE8S数据库连接出错，找不到Driver:" + e);
        }

        return c;
    }
}