package io.iec.edp.caf.databaseobject.rtmanager;

import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.databaseobject.api.service.IDatabaseObjectDeployService;
import io.iec.edp.caf.i18n.framework.api.event.LanguageEnabledEvent;
import org.springframework.context.ApplicationListener;

import java.util.List;

/**
 * DBO部署Event
 *
 * @author renmh
 */
public class DboEventHandle implements ApplicationListener<LanguageEnabledEvent> {

    @Override
    public void onApplicationEvent(LanguageEnabledEvent languageEnabledEvent) {
        List fieldSuffixs = languageEnabledEvent.getFieldSuffixs();
        IDatabaseObjectDeployService databaseObjectDeployService = SpringBeanUtils.getBean(IDatabaseObjectDeployService.class);
        databaseObjectDeployService.modifyI18nDatabaseObjectsByNewLanguage(fieldSuffixs);
    }
}
