/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.generator.method;

import io.iec.edp.caf.generator.item.ItemInfoGenerator;
import org.eclipse.jdt.core.dom.Block;

import io.iec.edp.caf.generator.baseInfo.ClassMethodGenerateInfo;
import io.iec.edp.caf.generator.baseInfo.MethodGenerateInfo;

/**
 * @author liu_wei
 */
public abstract class ClassMethodGenerator extends MethodGenerator {

    protected ClassMethodGeneratorContext getContext() {
        return (ClassMethodGeneratorContext) super.getContext();
    }

    //CreateMethodInfoContext
    @Override
    protected MethodGeneratorContext createMethodInfoContext() {
        ClassMethodGeneratorContext context = createClassMethodInfoContext();
        return context;
    }

    protected ClassMethodGeneratorContext createClassMethodInfoContext() {
        return new ClassMethodGeneratorContext();
    }

    @Override
    protected MethodGenerateInfo createMethodInfo() {
        return new ClassMethodGenerateInfo();
    }

    @Override
    protected final void doGenerate() {
        getContext().setMethodBody(buildMethodBody());
        boolean isConstructor = buildIsConstructor();

        getContext().setIsConstructor(isConstructor);
        getContext().setOverride(getIsOverride());
        super.doGenerate();
    }

    @Override
    protected final void afterGenerate() {
        if (parentGenerator != null)
            ((ItemInfoGenerator) parentGenerator).addChildSyntax(getContext().getMethodGenInfo().getMethodResult());
    }

    protected boolean getIsOverride() {
        return false;
    }

    protected abstract Block buildMethodBody();

    protected boolean buildIsConstructor() {
        return false;
    }

}
