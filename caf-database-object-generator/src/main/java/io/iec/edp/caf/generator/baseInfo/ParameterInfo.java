/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.generator.baseInfo;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;

/**
 * @author liu_wei
 */
public class ParameterInfo extends BaseGenerateInfo {

    private String paramName;

    public TypeInfo getParamType() {
        return paramType;
    }

    private TypeInfo paramType;

    private SingleVariableDeclaration var;

    public ParameterInfo() {
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        if (paramName == null || paramName.isEmpty())
            throw new RuntimeException("ParamName不能为空");
        this.paramName = paramName;
    }

    public void setParamType(TypeInfo paramType) {
        this.paramType = paramType;
    }

    @Override
    void generate() {

    }

    public SingleVariableDeclaration getVariable(AST ast) {
        var = ast.newSingleVariableDeclaration();
        var.setType(paramType.getType(ast));
        var.setName(ast.newSimpleName(paramName));
        return var;
    }

    @Override
    public boolean equals(Object obj) {
        ParameterInfo paramInfo = (ParameterInfo) obj;

        if (paramInfo == null)
            return false;
        if (this.paramName != paramInfo.paramName)
            return false;
        if (!this.paramType.equals(paramInfo.paramType))
            return false;

        return true;
    }
}
