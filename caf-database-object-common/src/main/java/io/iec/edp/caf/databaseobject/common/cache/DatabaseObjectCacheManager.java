/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.common.cache;

import io.iec.edp.caf.boot.context.CAFContext;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.databaseobject.api.entity.AbstractDatabaseObject;
import io.iec.edp.caf.databaseobject.api.entity.DatabaseObjectTable;
import io.iec.edp.caf.databaseobject.common.DatabaseObjectCommonUtil;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.EntityManager;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @author zhaoleitr
 */
@Slf4j
public class DatabaseObjectCacheManager {
    private static Map<String, AbstractDatabaseObject> dboCache;
    public static Map<String, DatabaseObjectTable> mappingCache = new ConcurrentHashMap<>();
    public static Map<String,String> idmappingCache = new ConcurrentHashMap<>();
    private static DatabaseObjectCacheManager instance;

    private Instant cacheInstant = Instant.now();
    private static ConcurrentHashMap<String, Instant> dboTimeCacheInstant = new ConcurrentHashMap<String, Instant>();
    /**
     * 缓存有效期5s（单位:秒）
     */
    private final int cacheIndate = 10;

    private Lock lock = new ReentrantLock();
    private Map<String, ReentrantReadWriteLock> locks = new HashMap<String, ReentrantReadWriteLock>();

    public static DatabaseObjectCacheManager getInstance() {
        if (instance == null) {
            instance = new DatabaseObjectCacheManager();
        }
        return instance;
    }

    private static EntityManager entityManager;

    private static EntityManager getEntityManager() {
        if (entityManager == null) {
            entityManager = SpringBeanUtils.getBean(EntityManager.class);
        }
        return entityManager;
    }

    private DatabaseObjectCacheManager() {
        dboCache = new HashMap<>();
    }

    private ReentrantReadWriteLock getLock(String id) {
        if (locks.containsKey(id)) {
            return locks.get(id);
        }
        lock.lock();// 上锁
        try {
            if (!locks.containsKey(id)) {
                ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
                locks.put(id, lock);
            }
            return locks.get(id);
        } catch (Exception e) {
            log.error("获取锁失败", e);
            return new ReentrantReadWriteLock();
        } finally {
            lock.unlock();
        }
    }

    /// <summary>
    /// 获取缓存信息
    /// </summary>
    /// <param name="key">主键值</param>
    /// <returns></returns>
    public AbstractDatabaseObject getDatabaseObjectContent(String key) {
        String dboId = key;
        //支持多租户
        key = key + CAFContext.current.getTenantId();
        if(key.contains("#mapping")){
            if(dboCache.get(key)!=null){
                return dboCache.get(key);
            }else{
                return null;
            }
        }
        if (dboTimeCacheInstant.containsKey(key)) {
            if (Instant.now().isAfter(dboTimeCacheInstant.get(key))) {
                AbstractDatabaseObject object = dboCache.get(key);
                if (object == null) {
                    return null;
                }
                //SQL执行放在锁之前，只锁缓存更新逻辑
                String sql = "select   lastModifiedTime   from   GSPDatabaseObject   where   id   =   :id";
                List dboList = getEntityManager().createNativeQuery(sql).setParameter("id", dboId).getResultList();

                ReentrantReadWriteLock sessionLock = getLock(key);
                sessionLock.writeLock().lock();
                try {
                    if (Instant.now().isAfter(dboTimeCacheInstant.get(key))) {
                        try {
                            if (dboList == null || dboList.size() == 0) {
                                //动态临时表直接放缓存，需要兼容
                                if (dboCache.containsKey(key)) {
                                    return dboCache.get(key);
                                }
                                return null;
                            }
                            if (!object.getLastModifiedTime().equals(((Timestamp) dboList.get(0)).toLocalDateTime())) {
                                return null;
                            }
                            dboTimeCacheInstant.put(key, Instant.now().plusSeconds(cacheIndate));
                            return dboCache.get(key);
                        } catch (Exception e) {
                            throw new RuntimeException("获取缓存失败", e);
                        }
                    }
                } finally {
                    sessionLock.writeLock().unlock();
                }
            }
            return dboCache.get(key);
        } else {
            ReentrantReadWriteLock sessionLock = getLock(key);
            sessionLock.writeLock().lock();
            try {
                dboTimeCacheInstant.put(key, Instant.now().plusSeconds(cacheIndate));
                return dboCache.get(key);
            } catch (Exception e) {
                throw new RuntimeException("获取缓存失败", e);

            } finally {
                sessionLock.writeLock().unlock();
            }
        }
    }


    /// <summary>
    /// 往缓存中添加信息
    /// </summary>
    /// <param name="key">主键值</param>
    /// <param name="value">缓存的信息</param>

    public void addDatabaseObjectContent(String key, AbstractDatabaseObject value) {
        key = key + CAFContext.current.getTenantId();
        dboCache.put(key, value);
    }


    /// <summary>
    /// 移除缓存信息
    /// </summary>
    /// <param name="key">主键值</param>

    public void removeDatabaseObjectContent(String key) {
        if(DatabaseObjectCommonUtil.isToolEntrance)
        {
            return;
        }
        key = key + CAFContext.current.getTenantId();
        dboCache.remove(key);
    }

    /**
     * 清空缓存信息
     */
    public void clearDatabaseObjectContent() {
        dboCache.clear();
    }

    /**
     * 移除一组缓存
     *
     * @param keys 主键
     */
    public void clearDatabaseObjectContentByIds(List<String> keys) {
        for (String item : keys) {
            item = item + CAFContext.current.getTenantId();
            dboCache.remove(item);
        }
    }

}
