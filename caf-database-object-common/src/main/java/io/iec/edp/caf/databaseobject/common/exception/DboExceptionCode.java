package io.iec.edp.caf.databaseobject.common.exception;

/**
 * @author xuefengping
 * @date 2023/5/22 18:20
 */
public class DboExceptionCode {
    /**
     *
     */
    public static final String JudgeFailed = "GSP_DBO_0001";
    /**
     *
     */
    public static final String DeleteDboRecordError = "GSP_DBO_0002";

}
