/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.rtmanager.rpc;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.databaseobject.api.entity.AbstractDatabaseObject;
import io.iec.edp.caf.databaseobject.api.entity.DboDeployResultInfo;
import io.iec.edp.caf.databaseobject.api.service.IDatabaseObjectDeployService;
import io.iec.edp.caf.databaseobject.api.service.IDatabaseObjectDtService;
import io.iec.edp.caf.databaseobject.api.service.IDatabaseObjectRtService;
import io.iec.edp.caf.databaseobject.common.DatabaseObjectCommonUtil;
import io.iec.edp.caf.databaseobject.rpcapi.service.DatabaseObjectRpcService;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author : liu_wei
 * @date : 2022-02-08 17:32
 **/
public class DatabaseObjectRpcServiceImpl implements DatabaseObjectRpcService {

    private IDatabaseObjectRtService databaseObjectRtService = SpringBeanUtils.getBean(IDatabaseObjectRtService.class);
    private IDatabaseObjectDeployService databaseObjectDeployService = SpringBeanUtils.getBean(IDatabaseObjectDeployService.class);
    private IDatabaseObjectDtService databaseObjectDtService = SpringBeanUtils.getBean(IDatabaseObjectDtService.class);

    @Override
    public AbstractDatabaseObject getDatabaseObject(String dboId) {
        return databaseObjectRtService.getDatabaseObject(dboId);
    }

    @Override
    public AbstractDatabaseObject getDatabaseObjectByCode(String dboCode) {
        return databaseObjectRtService.getDatabaseObjectByCode(dboCode);
    }

//    @Override
//    public List<AbstractDatabaseObject> getDatabaseObjectList() {
//        return databaseObjectRtService.getDatabaseObjectList();
//    }

    @Override
    public AbstractDatabaseObject getDtDatabaseObject(String dboId) {
        return databaseObjectDtService.getDtDatabaseObject(dboId);
    }

    @Override
    public AbstractDatabaseObject getDtDatabaseObjectByCode(String dboCode) {
        return databaseObjectDtService.getDtDatabaseObjectByCode(dboCode);
    }

    @Override
    public String saveDtDatabaseObject(String databaseObject) {
        try {
            DatabaseObjectCommonUtil commonUtil = new DatabaseObjectCommonUtil();
            AbstractDatabaseObject object = commonUtil.deserialze(databaseObject);
            databaseObjectDtService.saveDtDatabaseObject(object);
        } catch (Exception e) {
            throw new RuntimeException("RPC设计时DBO保存异常：" + e.getMessage(), e);
        }
        return "success";
    }

    @Override
    public String deleteDtDatabaseObject(String dboId) {
        databaseObjectDtService.deleteDtDatabaseObject(dboId);
        return "success";
    }

    @Override
    public boolean isExistDatabaseObjectDt(String dboId) {
        return databaseObjectDtService.isExistDatabaseObjectDt(dboId);
    }

    @Override
    public String deployDatabaseObject(String databaseObject, Map<String, String> dimensionInfo) {
        try {
            DatabaseObjectCommonUtil commonUtil = new DatabaseObjectCommonUtil();
            AbstractDatabaseObject object = commonUtil.deserialze(databaseObject);
            databaseObjectDeployService.deployDatabaseObject(object, dimensionInfo);
        } catch (Exception e) {
           throw new RuntimeException("RPC部署过程中异常：" + e.getMessage(), e);
        }
        return "success";
    }

    @Override
    public DboDeployResultInfo deployDatabaseObjects(String databaseObjects, Map<String, String> dimensionValue) {
        try {
            DatabaseObjectCommonUtil commonUtil = new DatabaseObjectCommonUtil();
            List<AbstractDatabaseObject> objects = new ArrayList<>();
            JSONArray content = JSONObject.parseArray(databaseObjects);
            for (Iterator iter = content.iterator(); iter.hasNext();) {
                String object = iter.next().toString();
                objects.add(commonUtil.deserialze(object));
            }
            return databaseObjectDeployService.deployDatabaseObjects(objects, dimensionValue);
        } catch (Exception e) {
            throw new RuntimeException("RPC部署过程中异常：" + e.getMessage(), e);
        }
    }
}
