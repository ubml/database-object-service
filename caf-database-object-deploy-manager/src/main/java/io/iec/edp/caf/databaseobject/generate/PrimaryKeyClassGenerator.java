/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.generate;

import io.iec.edp.caf.generator.baseInfo.AnnotationInfo;
import io.iec.edp.caf.generator.baseInfo.AnnotationType;
import io.iec.edp.caf.generator.baseInfo.CompilationUnitInfo;
import io.iec.edp.caf.generator.baseInfo.TypeInfo;
import io.iec.edp.caf.generator.field.FieldGenerator;
import io.iec.edp.caf.generator.item.ClassGenerator;
import io.iec.edp.caf.generator.item.ClassGeneratorContext;
import io.iec.edp.caf.generator.method.ClassMethodGenerator;
import io.iec.edp.caf.databaseobject.api.entity.DbType;
import io.iec.edp.caf.databaseobject.api.entity.DatabaseObjectColumn;
import io.iec.edp.caf.databaseobject.api.entity.DatabaseObjectTable;
import org.eclipse.jdt.core.dom.Modifier;

import java.util.ArrayList;

/**
 * @author liu_wei
 */
public class PrimaryKeyClassGenerator extends ClassGenerator {
    protected PrimaryKeyClassGenerator(CompilationUnitInfo projectInfo) {
        super(projectInfo);
    }

    private DatabaseObjectTable table;
    private DbType dbType;

    public DbType getDbType() {
        return dbType;
    }

    public void setDbType(DbType dbType) {
        this.dbType = dbType;
    }

    public void setAbstractDatabaseObject(DatabaseObjectTable table) {
        this.table = table;
    }

    public DboClassGenerateContext dboClassGenerateContext;

    @Override
    protected ClassGeneratorContext createClassInfoContext() {

        PrimaryKeyClassGeneratorContext context = new PrimaryKeyClassGeneratorContext();
        context.setDabaseObject(this.table);
        return context;
    }

    @Override
    public ArrayList<TypeInfo> addParentInterfaces() {
        ArrayList<TypeInfo> arrayList = new ArrayList<>();
        arrayList.add(new TypeInfo("Serializable"));
        return arrayList;
    }

    @Override
    protected ClassGeneratorContext getContext() {
        return super.getContext();
    }

    @Override
    protected ArrayList<Modifier.ModifierKeyword> getAccessModifier() {
        ArrayList<Modifier.ModifierKeyword> arrayList = new ArrayList<>();
        arrayList.add(Modifier.ModifierKeyword.PUBLIC_KEYWORD);
        return arrayList;
    }

    @Override
    protected String getName() {
        return table.getCode() + "Id";
    }

    @Override
    protected ArrayList<ClassMethodGenerator> createConstructorGenerators() {
        ArrayList<ClassMethodGenerator> arrayList = new ArrayList<>();

        PrimaryKeyClassMethodWithoutParamGenerator generator = new PrimaryKeyClassMethodWithoutParamGenerator(table);
        arrayList.add(generator);
        PrimaryKeyClassMethodGenerator classMethodGenerator = new PrimaryKeyClassMethodGenerator(table);
        arrayList.add(classMethodGenerator);
        return arrayList;
    }

    @Override
    public ArrayList<FieldGenerator> createFieldGenerator() {
        ArrayList<FieldGenerator> fieldGenerators = new ArrayList<>();
        for (String primaryKey : table.getPrimaryKey()) {
            DatabaseObjectColumn column = table.getColumnById(primaryKey);
            column.setIfPrimaryKey(false);
            PrimaryKeyFieldGenerator generator = new PrimaryKeyFieldGenerator(column, table);
            fieldGenerators.add(generator);
        }
        return fieldGenerators;
    }

    @Override
    public ArrayList<AnnotationInfo> getAttributeList() {
        ArrayList<AnnotationInfo> annotationInfos = new ArrayList<AnnotationInfo>();
        annotationInfos.add(GetEmbeddableAnntation());
        return annotationInfos;
    }

    private AnnotationInfo GetEmbeddableAnntation() {
        AnnotationInfo annotationInfo = new AnnotationInfo();
        annotationInfo.setAnnotationType(AnnotationType.Marker);
        annotationInfo.setTypeInfo(new TypeInfo("Embeddable"));
        return annotationInfo;
    }

    @Override
    protected ArrayList<ClassMethodGenerator> createMethodGenerators() {
        return super.createMethodGenerators();
    }
}
