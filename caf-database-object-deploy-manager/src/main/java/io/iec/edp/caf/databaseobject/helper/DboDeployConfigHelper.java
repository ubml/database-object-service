/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.helper;

import io.iec.edp.caf.databaseobject.api.configuration.deplpyconfig.DboDeployConfiguration;
import io.iec.edp.caf.databaseobject.api.configuration.deplpyconfig.DboDeployConfigurationLoader;

import java.io.IOException;

/**
 * @author liu_wei
 */
public class DboDeployConfigHelper extends DboDeployConfigurationLoader {
    private static DboDeployConfigHelper instance;

    /**
     * 元数据序列化器帮助
     */

    public static DboDeployConfigHelper getInstance() {
        return (instance != null) ? instance : (instance = new DboDeployConfigHelper());
    }

    /**
     * 构造器
     */
    private DboDeployConfigHelper() {

    }

    /**
     * 返回各类型数据库部署配置
     *
     * @return
     */
    public final DboDeployConfiguration getObjectNameMaxLengthConfiguration() {
        return getDatabaseObjectConfiguration();
    }
}
