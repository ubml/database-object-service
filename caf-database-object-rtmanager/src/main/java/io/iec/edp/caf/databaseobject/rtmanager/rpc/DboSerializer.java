/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.rtmanager.rpc;

import io.iec.edp.caf.databaseobject.api.entity.AbstractDatabaseObject;
import io.iec.edp.caf.databaseobject.common.DatabaseObjectCommonUtil;
import io.iec.edp.caf.rpc.api.serialize.RpcCustomSerializer;
import org.apache.commons.lang3.StringUtils;

/**
 * @author : liu_wei
 * @date : 2022-02-14 10:44
 **/
public class DboSerializer implements RpcCustomSerializer {
    @Override
    public String serialize(Object o) {
        DatabaseObjectCommonUtil commonUtil = new DatabaseObjectCommonUtil();
        try
        {
            return commonUtil.serialze((AbstractDatabaseObject) o);
        }
        catch (Exception e)
        {
            throw new RuntimeException("序列化DBO出错：" + e.getMessage(), e);
        }
    }

    @Override
    public <T> T deserialize(String s, Class<T> aClass) {
        DatabaseObjectCommonUtil commonUtil = new DatabaseObjectCommonUtil();
        try
        {
            if(s == null || ("null").equalsIgnoreCase(s) || StringUtils.isBlank(s))
            {
                return null;
            }
            return (T)commonUtil.deserialze(s);
        }
        catch (Exception e)
        {
            throw new RuntimeException("反序列化DBO出错：" + e.getMessage(), e);
        }
    }
}
