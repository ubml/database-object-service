/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.generator.baseInfo;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.AnnotationTypeMemberDeclaration;
import org.eclipse.jdt.core.dom.Modifier.ModifierKeyword;

/**
 * @author liu_wei
 */
public class AnnotationMemberInfo extends BaseGenerateInfo {
    private TypeInfo type;
    private String name;
    private List<ModifierKeyword> modifiers;

    protected AnnotationTypeMemberDeclaration memberResult;

    public AnnotationMemberInfo() {
        modifiers = new ArrayList<ModifierKeyword>();
    }

    public AnnotationTypeMemberDeclaration getMemberResult() {
        return memberResult;
    }

    public void setType(TypeInfo type) {
        this.type = type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setModifiers(List<ModifierKeyword> modifiers) {
        this.modifiers = modifiers;
    }

    @Override
    public void generate() {
        memberResult = ast.newAnnotationTypeMemberDeclaration();

        memberResult.setName(ast.newSimpleName(name));
        memberResult.setType(type.getType(ast));
        if (modifiers != null) {
            for (ModifierKeyword modifier : modifiers) {
                memberResult.modifiers().add(ast.newModifier(modifier));
            }
        }
    }
}
