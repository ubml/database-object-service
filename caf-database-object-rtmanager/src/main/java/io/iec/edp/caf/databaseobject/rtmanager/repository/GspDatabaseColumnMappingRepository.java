package io.iec.edp.caf.databaseobject.rtmanager.repository;

import io.iec.edp.caf.data.orm.DataRepository;
import io.iec.edp.caf.databaseobject.api.entity.GspDatabaseObjectColumnMapping;

import java.util.List;

public interface GspDatabaseColumnMappingRepository extends DataRepository<GspDatabaseObjectColumnMapping, String> {
    List<GspDatabaseObjectColumnMapping> findAllByDboId(String dboId);
}
