/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.common.database;

import io.iec.edp.caf.databaseobject.api.entity.DBInfo;

/**
 * @author liu_wei
 */
public class GspDbFactory {

    public static GspDatabase getDatabase(DbConfigData configData) {
        switch (configData.getDbType()) {
            case DM:
                return new DMDatabase(configData);
            case PgSQL:
                return new PostgreSQLDatabase(configData);
            case Oracle:
                return new OracleDatabase(configData);
            case SQLServer:
                return new SQLServerDatabase(configData);
            case HighGo:
                return new HighGoDatabase(configData);
            case MySQL:
                return new MySQLDatabase(configData);
            case Oscar:
                return new OscarDatabase(configData);
            case Kingbase:
                return new KingbaseDatabase(configData);
            case DB2:
                return new DB2Database(configData);
            case OpenGauss:
                return new OpenGaussDatabase(configData);
            default:
                throw new RuntimeException("数据库类型不正确:" + configData.getDbType());
        }
    }

    public static GspDatabase getDatabase(DBInfo dbInfo) {

        //数据库连接
        DbConfigData configData = new DbConfigData();
        configData.setDbType(dbInfo.getDbType());
        configData.setConnectionString(dbInfo.getUrl());
        configData.setUserId(dbInfo.getUserName());
        configData.setPassword(dbInfo.getPassWord());

        return getDatabase(configData);
    }
}
