/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.rtmanager.config;







import io.iec.edp.caf.databaseobject.api.service.IDatabaseObjectDeployService;
import io.iec.edp.caf.databaseobject.api.service.IDatabaseObjectDtService;
import io.iec.edp.caf.databaseobject.api.service.IDatabaseObjectRtService;
import io.iec.edp.caf.databaseobject.api.service.IDatabaseObjectTempTableService;
import io.iec.edp.caf.databaseobject.rpcapi.service.DatabaseObjectRpcService;
import io.iec.edp.caf.databaseobject.rtmanager.*;
import io.iec.edp.caf.databaseobject.rtmanager.repository.GspDatabaseColumnMappingRepository;
import io.iec.edp.caf.databaseobject.rtmanager.repository.GspDatabaseObjectDtRepository;
import io.iec.edp.caf.databaseobject.rtmanager.repository.GspDatabaseObjectMappingRepository;
import io.iec.edp.caf.databaseobject.rtmanager.repository.GspDatabaseObjectRepository;
import io.iec.edp.caf.databaseobject.rtmanager.rpc.DatabaseObjectRpcServiceImpl;
import io.iec.edp.caf.rest.RESTEndpoint;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @author liu_wei
 */
@Configuration
@EnableJpaRepositories("io.iec.edp.caf.databaseobject.rtmanager.repository")
@EntityScan("io.iec.edp.caf.databaseobject.api.entity")
public class DatabaseObjectRtServiceConfiguration {
    @Bean
    public IDatabaseObjectRtService databaseObjectRtService(
            GspDatabaseObjectRepository repository, GspDatabaseObjectMappingRepository mappingRepository, GspDatabaseColumnMappingRepository columnMappingRepository
            ) {
        return new DatabaseObjectRtService(repository,mappingRepository,columnMappingRepository);
    }

    @Bean
    public IDatabaseObjectDtService databaseObjectVersionService(
            GspDatabaseObjectDtRepository repo
    ) {
        return new DatabaseObjectDtServiceImpl(repo);
    }


    @Bean
    public RESTEndpoint databaseObjectsRTEndpoint(IDatabaseObjectRtService databaseObjectRtService) {
        return new RESTEndpoint(
                "/runtime/sys/v1.0/database-objects",
                databaseObjectRtService
        );
    }

    @Bean
    public IDatabaseObjectDeployService databaseObjectDeployService() {
        return new DatabaseObjectDeployServiceImpl();
    }

    @Bean
    public DboEventHandle returnDboEventHandle() {
        return new DboEventHandle();
    }

    @Bean
    public DatabaseObjectRpcService databaseObjectRpcService()
    {
        return new DatabaseObjectRpcServiceImpl();
    }

    @Bean
    public IDatabaseObjectTempTableService databaseObjectTempTableService(){
        return new DatabaseObjectTempTableServiceImpl();
    }
}
