/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.generator.baseInfo;

import java.util.ArrayList;

import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.Modifier.ModifierKeyword;

import io.iec.edp.caf.generator.method.GetterMethodGenerator;
import io.iec.edp.caf.generator.method.SetterMethodGenerator;

/**
 * @author liu_wei
 */
public class ClassPropertyGenerateInfo extends PropertyGenerateInfo {

    private ArrayList<ModifierKeyword> getterModifiers;
    private ArrayList<ModifierKeyword> setterModifiers;
    private Block getterBlock;
    private Block setterBlock;
    private boolean isGetterOverride = false;
    private boolean isSetterOverride = false;
    // region Getter/Setter

    public void setGetterModifiers(ArrayList<ModifierKeyword> getterModifiers) {
        this.getterModifiers = getterModifiers;
    }

    public void setSetterModifiers(ArrayList<ModifierKeyword> setterModifiers) {
        this.setterModifiers = setterModifiers;
    }

    public void setGetterBlock(Block getterBlock) {
        this.getterBlock = getterBlock;
    }

    public void setSetterBlock(Block setterBlock) {
        this.setterBlock = setterBlock;
    }

    public void setGetterOverride(boolean isGetterOverride) {
        this.isGetterOverride = isGetterOverride;
    }

    public void setSetterOverride(boolean isSetterOverride) {
        this.isSetterOverride = isSetterOverride;
    }

    // endregion
    @Override
    public void generate() {
        propResult = new ArrayList<MethodDeclaration>();
        if (hasGetMethod)
            propResult.add(getGetterDeclaration());
        if (hasSetMethod)
            propResult.add(setGetterDeclaration());

    }

    private MethodDeclaration getGetterDeclaration() {
        GetterMethodGenerator getterGenerator = new GetterMethodGenerator(propertyName, fieldName, propertyType,
                getterModifiers, gettrtAttributes, isGetterOverride, getterBlock);
        getterGenerator.setAst(ast);
        getterGenerator.initialize();
        getterGenerator.generate();
        return getterGenerator.getResult();
    }

    private MethodDeclaration setGetterDeclaration() {
        SetterMethodGenerator setterGenerator = new SetterMethodGenerator(propertyName, fieldName, propertyType,
                setterModifiers, settrtAttributes, isSetterOverride, setterBlock);
        setterGenerator.setAst(ast);
        setterGenerator.initialize();
        setterGenerator.generate();
        return setterGenerator.getResult();
    }
}
