package io.iec.edp.caf.databaseobject;

/**
 * @author： Tom li
 * @since： 2022/5/2
 */

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.List;

/**
 * @author： Tom li
 * @since： 2022/5/1
 */
@Data
public class DboDeployRequest {
    private String dboFilePath;
    private String dbType;
    private String url;
    private String user;
    private String password;
    private List<String> fiYearList;
    private boolean isDbSetup;

    @JsonIgnore
    public String[] convertToArgs(){
        String []strs=new String[6];
        strs[0]=dboFilePath;
        strs[1]=dbType;
        strs[2]=url;
        strs[3]=user;
        strs[4]=password;
        strs[5]= JSON.toJSONString(fiYearList);
        return strs;
    }
}

