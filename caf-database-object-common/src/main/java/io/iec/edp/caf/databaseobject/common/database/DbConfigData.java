/*
 * Copyright © OpenAtom Foundation.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package io.iec.edp.caf.databaseobject.common.database;

import io.iec.edp.caf.databaseobject.api.entity.DbType;
import lombok.Data;

/**
 * @author liu_wei
 */
@Data
public class DbConfigData {

    private int defaultconnecttimeout = 7200;
    private int defaultcommandtimeout = 7200;
    private int defaultmaxPoolSize = 100;

    /**
     * 构造函数
     */
    public DbConfigData() {

    }

    /**
     * 数据库连接池中的id
     */
    private String connectionId;

    /**
     * 数据库配置编号
     */
    private String code;

    /**
     * 数据库类型描述
     */
    private DbType dbType;

    /**
     * 用户名
     */
    private String userId;

    /**
     * 明文口令
     */
    private String password;

    /**
     * 数据库地址
     */
    private String serverIp;

    /**
     * 数据库端口号
     */
    private String port;

    /**
     * 服务名
     */
    private String serviceName;

    /**
     * 服务提供者
     */
    private String provider;

    /**
     * 指代数据库名
     */
    private String catalog;

    /**
     * 指定数据库连接TimeOut
     */
    private int connectTimeout;

    /**
     * 指定sql执行TimeOut
     */
    private int commandTimeout;

    /**
     * 指定连接池最大连接数 默认值为100
     */
    private int maxPoolSize;

    /**
     * 数据库配置描述
     */
    private String description;

    /**
     * 数据库连接字符串
     */
    private String connectionString;

}
